
Commercialization Plan

Draft


Group 3

9/2/2013





 
 INTRODUCTION


This report was commissioned by Dr Russell Johnson � Massey University. The purpose of this 
report is to develop a commercialization plan that would market the Tic-Tac-Toe game created 
as the main component of the Capstone Project in 2013.

The Capstone Project makes up the paper itself for semester two of the third year. It is 
designed to train students to work actively in teams as required in the fields. Students 
will come to realize that allocated tasks are inter-linked, which means delaying some 
parts will delay the whole project.

This report analyses the ways in which the game will be communicated to the general public 
and shows how revenues could be generated from the project

Marketing is the fundamental element which will be elaborated on in this report. It is the 
process of communicating the value of a product to customers, for the purpose of selling 
it. It is an important business utility for attracting customers. Developing the methods 
of communicating the online game to the clients is very essential in the whole process of 
commercializing it. 

According to the project outline, the game will be basically played online - in the public 
domain. That is, it will be for social and at the same time generate revenue. �Since the 
early 2000s, the development and growth of online games have been phenomenal. Online games 
once recognized as a mere method of entertainment have proven potential for profitability 
and is a viable business model that guarantees profit� [5]. However, getting the clients 
to like it and actively engage in it is not as easy. 



COMMERCIALIZATION METHODS

1.1	ADVERSTISING

Advertising is a type of communication which is normally used in marketing and it encourage, 
persuade, or manipulate an audience to continue or take some new actions. Generally, the 
desired result is to drive consumer behavior with respect to a commercial offering. To 
communicate the Tic-Tac-Toe game to the general public, the group will be using Google 
Adsense and In-game Advertising. 

1.1.1	Google Adsense

The most popular way to advertise on a webpage is to use the Google Adsense [8], hence the 
choice. There are two approaches to go about this: Non-full-screen games and Full-screen games. 
Both will be used in our case.

1.1.1.1	Non-full-screen games

With this, we just need to set up our account, get our site approved, and then add a little 
block of code which generates adverts from the sites that people using adwords have created. 
This will generate ads around the webpage.

1.1.1.2	Full-screen games

This would be applicable in the pause menu of the HTML5 game. This approach works for both 
full-screen games and non-full-screen games. The method of doing this is:
		Put the ad's code inside a <div id="whatever" style="display: none;"></div>. This will load 
		the ad, but it will be invisible. In the game's code, we just have to make it that when the 
		game is paused, it makes it appear.

		For instance:

		document.getElementById('whatever').style.display="";
		Then when the game is unpaused, we use:
		document.getElementById('whatever').style.display="none";

These advertisements will be managed, sorted and maintained by Google, and they can generate 
revenue on either a per-click (pay per click) or per-impression (cost per impression) basis [9]. 


1.1.2		In-game Advertising

In-game advertising involves advertising in computer or video games. It can be incorporated into 
the game through various ways, two of which use the background display, such as billboard and 
commercial during the pause created when a game loads.  In the Tic-Tac-Toe game�s case, this 
approach will be catered for in the stacked 3D TTT. 


1.1.2.1	Dynamic advertising

Dynamic in-game advertising merges the customization of web banners with the traditional billboards 
and posters� functionality since most in-game advertisements do not link to a website outside the 
game.  This approach will allow our group (game developer) or the advertisement delivery service to 
track advertisements in real time and capture viewing data such as screen time, type of advertisement, 
and viewing angle [4]. Dynamic advertisements can be purchased after our Tic-Tac-Toe game is released 
to the general public is featured in our group�s determined in-game location. An ideal example for this 
scenario is the �purchase of the billboard advertisements in 10 swing states by the then US Democratic 
Presidential candidate Barack Obama in numerous Xbox games in October 2008� [4].


1.1.2.2	Static advertising

Static in-game advertising operates like the concept of product placement in film industries, whereby, 
it cannot be altered once they are programmed as they are programmed directly into the game (unless it 
is completely online). Nevertheless, Static in-game advertising allows gamers to interact with the virtual 
products [4]. Unfortunately, this approach is not applicable in our tic-tac-toe game.


1.2	FACEBOOK (SOCIAL NETWORK)

As the Facebook Platform economy has matured over the past few years, it�s become clear that virtual 
goods have become the principal source of revenue for application developers. The Facebook Platform is 
enabling games to reach a much bigger audience than will ever play on consoles � most social game players 
would not even consider themselves �gamers�. We will be focusing on generating revenue through the virtual 
items - which is of high expectation as Facebook is currently one of the largest and fast growing social 
networks [6].


1.2.1		Decorative Items

Decorative virtual goods are items such as accessories for avatars, goods for virtual homes or profile 
themes. We will be allowing for the gamers to change their avatars and profile themes to suit their 
preference and pay for those virtual items every time they use them.
These decorative items generally allow users more customized forms of self expression than they would 
otherwise have access to. Researchers show that �allowing individuals more choices concerning their 
avatars should increase their feelings of identification with the avatars� [3], hence integrate into 
the user�s idea of self during gameplay. This item therefore, has a high potential of generating 
revenue from our tic-tac-toe game.


1.3	ADVANCE FEATURES

This section of the commercialization focuses on the additional features that the tic-tac-toe game 
would have. These features would increase the overall gaming environment; increase the level of 
complexity which implies competitiveness and offers additional tools for support during the gameplay 
challenge.


1.3.1	Charging for Advance Features

In order to obtain those advance features the team has to allow for micro-transactions: Credit 
card (VISA) and PayPal in the system to do the online purchases.


1.3.1.1	Free to play normal Tic-Tac-Toe

With this, it will be free-to-play normal tic-tac-toe but players have to pay to play other 
versions. This approach will be a form of attraction and also getting the gamers familiar 
with the game itself, and boosts the revenue. According to researchers, �The rapidly expanding 
��free-to-play�� online game payment model represents a huge shift in digital game 
commercialization, with cash payments for virtual items increasingly recognized as central to 
��free game�� participation� [7].


1.3.1.2	Limits to Free-To-Play normal Tic-Tac-Toe

This approach will allow for gamers to play 3 games for free per day, and pay to do more. 
This also has the potential to increase the revenue as players would want to do more 
after the 3rd game is over. Playing only 3 times is really not satisfactory especially 
in the case of the tic-tac-toe.


1.3.1.3	Play normal Tic-Tac-Toe for free for a certain period

This option allows for gamers to play the tic-tac-toe for free for a year before releasing 
the original version with advance features, which will be paid for either online or through 
the subscription payments. 



CONCLUSION

2.1.1	Google Adsense will be used effectively to communicate the tic-tac-toe game to the 
	general public. Both approaches will be used: Non-full-screen games and Full-screen 
	games
	
2.1.2	In-game advertising will be catered for in the stacked 3D TTT. It will utilize the 
		Dynamic advertising but not the Static advertising style.

2.2.1	Decorative virtual goods are the main items that will be on the market for 
		generating revenue for our tic-tac-toe game.

2.3.1	Mirco-transactions: Credit card (VISA) and PayPal will be used for purchases 
		of advance Features of the game. Free to play normal tic-tac-toe but players 
		have to pay to play other versions; Free to play 3 games a day but players have 
		to pay to do more; and play for free for a year before releasing the original 
		version with advance features which will be paid for either online or through 
		the subscription payments, are the additional features for our tic-tac-toe game.
 	
	




REFERENCES

[1]	Adsense. (2011, June 6). In Wikipedia, The Free Encyclopedia. Retrieved August 28,
		2013, from  http://en.wikipedia.org/wiki/AdSense                                 

[2]	Advertising. In Wikipedia, The Free Encyclopedia. Retrieved August 30, 2013, from 
		http://en.wikipedia.org/wiki/Advertising

[3]	Bailey, R. , Wise, K., & Bolls, P. (2009). How avatar customizability affects children�s 
		arousal and subjective presence during junk food � sponsored online video games. 
		CyberPsychology & Behavior. 12(3), 277 � 283.

[4]	In-game advertising. In Wikipedia, The Free Encyclopedia. Retrieved August 26, 2013, 
		from http://en.wikipedia.org/wiki/In-game_advertising#Advertising_in_online-only_games

[5]	Lee, C. (2010). Influential factors of player�s loyalty toward online games for achieving 
		commercial success. Australasian Marketing Journal. 18(2010) 81 � 92.

[6]	Smith, J. (2009). Virtual goods and games as a service: Money for nothing, or here to 
		stay? Retrieve from http://www.insidefacebook.com/2009/12/01/virtual-goods-and-games-
		as-a-service-money-for-nothing-or-here-to-stay/


[7]	Sun, C. & Lin, H. (2010). Cash Trade in free-to-play online game. Games and Culture. 
		6(270), 271 � 287.
	
[8]	Murphy, D. (2005, September 5). Google�s ad network spreads the wealth. PC Magazine, 
		74(3), 3 � 12.

[9]	Wilson, S. (2011). How to make money from an HTML5 Game. Retrieved from 
		http://www.slashgame.net/2011/09/how-to-make-money-from-your-games.html






