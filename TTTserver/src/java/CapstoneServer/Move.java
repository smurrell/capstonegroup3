/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapstoneServer;

import java.util.Date;

/**
 *
 * @author Sam
 */
public class Move {

    private String playerID;
    private String nickName;
    private int i, j, x, y;
    private Date timestamp;
    
    public Move(String playerID, String nickName, int i, int j, int x, int y) {
        this.playerID = playerID;
        this.nickName = nickName;
        this.i = i;
        this.j = j;
        this.x = x;
        this.y = y;
        timestamp = new Date();
    }

    public String getMoveData() {
        return "[" + i + ", " + j + ", " + x + ", " + y + "]";
    }

    public String getPlayerID() {
        return playerID;
    }

    public String getNickName() {
        return nickName;
    }

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Date getTimestamp() {
        return timestamp;
    }
    
    public String getSubGameKey() {
        StringBuilder moveKey = new StringBuilder();
        return moveKey.append(i).append(j).toString();
    }

    @Override
    public String toString() {
        return "Move{" + "playerID=" + playerID + ", nickName=" + nickName + ", i=" + i + ", j=" + j + ", x=" + x + ", y=" + y + '}';
    }
}
