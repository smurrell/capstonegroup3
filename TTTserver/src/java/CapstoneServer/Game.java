/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapstoneServer;

import AuthServer.Player;
import Bots.BotManager;
import com.google.gson.Gson;
import java.util.Date;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

/**

 @author Sam
 */
public class Game {

    private String gameID;
    private Player player1;       // always crosses
    private Player player2;       // always noughts
    private Player creator;
    private String winner;          // store session ID of the winner, or "tie"
    private String currentTurn;     // will store the currentSessionID of the person whose turn it is, NOT THEIR DISPLAY NAME
    private String nickName = null;
    private int[][][][] gameArray;      //0 unused, 1 player1 (crosses), 2 player2 (noughts)
    private int[][] gameArraySuper;     // stores the winning symbol for all 9 subgames
    private int currentSubgameI;       // stores the i co-ordinate for the next subgame board
    private int currentSubgameJ;       // stores the j co-ordinate for the next subgame board
    private Move lastMove;
    private ArrayList<Move> moves;
    private String gameType; //values: public, private, <botId>
    private Boolean active;
    Date timestamp;
    Date joinTimestamp;

    public Game(Player firstPlayer, String gameType) {
        this.setup(firstPlayer, gameType);
    }

    public final void setup(Player firstPlayer, String gameType) {
        moves = new ArrayList<Move>();
        this.gameType = gameType;
        this.active = true;
        System.out.println("Game " + gameType);

        currentSubgameI = currentSubgameJ = 1;

        timestamp = new Date();

        creator = firstPlayer;
        Random rnd = new Random();
        if (rnd.nextBoolean() == true) {
            player1 = firstPlayer;
        }
        else {
            player2 = firstPlayer;
        }

        lastMove = new Move(null, null, 1, 1, 1, 1);

        gameID = UUID.randomUUID().toString();

        int n = 3;
        gameArray = new int[n][n][n][n];
        gameArraySuper = new int[n][n];
        System.out.println(player1);
        System.out.println(player2);
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public boolean isPlayer1(String sID) {
        return sID.equals(player1.getCurrentSessionID());
    }

    public boolean isPlayer2(String sID) {
        return sID.equals(player2.getCurrentSessionID());
    }

    // return JSON data to send through an SSE connection
    public String returnJSON() {
        Gson gson = new Gson();
        String boardArray = gson.toJson(gameArray, gameArray.getClass());

        String JSON = "data: {";

        JSON += "\"gameArray\" : " + boardArray + ", ";
        JSON += "\"currentTurn\" : \"" + this.currentTurn + "\", ";
        JSON += "\"lastMove\" : " + lastMove.getMoveData() + ", ";

        //String nickName = null;
        if (!isSinglePlayer()) {
            Player opponentPlayer = player1;
            if (player1 != getPlayer(currentTurn)) {
                opponentPlayer = player2;
            }
            nickName = opponentPlayer.getNickName();
            if (nickName == null) {
                nickName = opponentPlayer.getFirstName();
            }
        }
        else {
            nickName = BotManager.getBotName(gameType);
        }
        JSON += "\"opponent\" : \"" + nickName + "\" ,";
        JSON += "\"active\" : " + active;

        JSON += "}\n\n";
        return JSON;
    }

    public Player getCreator() {
        return creator;
    }

    public boolean validateMove(Move newMove) {
        boolean valid = true;
        
        int i = newMove.getI(), j = newMove.getJ();
        int x = newMove.getX(), y = newMove.getY();

        int targetI = this.currentSubgameI;
        int targetJ = this.currentSubgameJ;

        if (gameArraySuper[targetI][targetJ] == 0) {
            if ((i != targetI | j != targetJ)
                    || gameArray[i][j][x][y] != 0) {
                valid = false;
            }
        }
        if (gameArraySuper[i][j] != 0 || gameArray[i][j][x][y] != 0) {
            valid = false;
        }
        return valid;
    }

    public boolean isPublicOpen() {
        if (gameType.equals("public")) {
            if (player2 == null ^ player1 == null) {
                return true;
            }
        }

        return false;
    }

    public void joinGame(Player secondPlayer) {
        joinTimestamp = new Date();
        if (player1 == null) {
            player1 = secondPlayer;
        }
        else {
            player2 = secondPlayer;
        }

        currentTurn = player1.getCurrentSessionID();
    }

    public int getMoveCount() {
        return this.moves.size();
    }

    public boolean addMove(Move newMove) {
        boolean validMove = false;

        // DEBUG PRINTING
        for (int i = 0; i < 3; i++) {
            System.out.print("[");
            for (int j = 0; j < 3; j++) {
                System.out.print(gameArraySuper[i][j] + " ");
            }
            System.out.println("]");
        }

        if (validateMove(newMove)) {
            System.out.println("Move validated!");

            lastMove = newMove;
            validMove = true;
            moves.add(newMove);

            int i = newMove.getI();
            int j = newMove.getJ();
            int x = newMove.getX();
            int y = newMove.getY();

            this.currentSubgameI = x;
            this.currentSubgameJ = y;

            // swap the current turn
            if (currentTurn.equals(player1.getCurrentSessionID())) {
                gameArray[i][j][x][y] = 1;
                currentTurn = player2.getCurrentSessionID();
            }
            else {
                gameArray[i][j][x][y] = 2;
                currentTurn = player1.getCurrentSessionID();
            }

            gameArraySuper[i][j] = this.isWinningSubgame(i, j);

            resolveGameBoard();
            // debug printing
            for (int p = 0; p < 3; p++) {
                System.out.print("[");
                for (int q = 0; q < 3; q++) {
                    System.out.print(gameArraySuper[p][q] + " ");
                }
                System.out.print("]\t");
                System.out.println("[" + gameArray[i][j][p][0] + " " + gameArray[i][j][p][1] + " " + gameArray[i][j][p][2] + "]");
            }
            System.out.println("\n");

        }
        else {
            System.out.println("Invalid move!");
        }

        if (!gameType.substring(0, 1).equals("p")) {
        }

        return validMove;
    }

    private void resolveGameBoard() {
        if (checkForWinner() == false) {
            checkForTies();
        }
    }

    public ArrayList<Move> getMoves() {
        return moves;
    }

    public boolean checkForTies() {

        boolean isATie = false;

        if (moves.size() >= 81) {
            isATie = true;
        }

        return isATie;

    }

    public int[][][][] getGameBoard() {
        return this.gameArray;
    }

    // returns the symbol
    public int isWinningSubgame(int i, int j) {
        int winningSymbol = 0;  // 0 is the value for no winner
        for (int k = 0; k < 3; k++) {
            if (gameArray[i][j][k][0] != 0
                    && (gameArray[i][j][k][1] == gameArray[i][j][k][0] && gameArray[i][j][k][2] == gameArray[i][j][k][1])) {
                winningSymbol = gameArray[i][j][k][0];  // get the winning symbol
                System.out.println("Row - Obtained a winning symbol - " + winningSymbol);
                break;
            }

            if (gameArray[i][j][0][k] != 0
                    && (gameArray[i][j][1][k] == gameArray[i][j][0][k] && gameArray[i][j][2][k] == gameArray[i][j][1][k])) {
                winningSymbol = gameArray[i][j][0][k];  // get the winning symbol
                System.out.println("Column - Obtained a winning symbol - " + winningSymbol);
                break;
            }
        }

        if (winningSymbol == 0) {
            if (gameArray[i][j][1][1] != 0) {
                if ((gameArray[i][j][0][0] == gameArray[i][j][1][1] && gameArray[i][j][1][1] == gameArray[i][j][2][2])
                        || (gameArray[i][j][0][2] == gameArray[i][j][1][1] && gameArray[i][j][1][1] == gameArray[i][j][2][0])) {
                    winningSymbol = gameArray[i][j][1][1];
                    System.out.println("Diagonal - Obtained a winning symbol - " + winningSymbol);
                }
            }
        }

        return winningSymbol;
    }

    public boolean checkForWinner() {
        boolean gameFinished = false;

        //this.checkForWinningSubgames();

        int[] primes = {0, 2, 3, 5};

        // check the rows and columns

        int columnProduct, rowProduct, winningSymbol = 0;

        for (int k = 0; k < 3; k++) {
            rowProduct = primes[gameArraySuper[k][0]] * primes[gameArraySuper[k][1]] * primes[gameArraySuper[k][2]];
            columnProduct = primes[gameArraySuper[0][k]] * primes[gameArraySuper[1][k]] * primes[gameArraySuper[2][k]];

            // k-th column has no zeros in it
            if (columnProduct != 0) {
                // check that a Nought and a Cross are not together in this column (apart from the tie symbol)
                if ((columnProduct % 2 == 0 ^ columnProduct % 3 == 0) && columnProduct != 125) {
                    for (int i = 0; i < 3; i++) {
                        if (gameArraySuper[0][i] != 3) // if the current symbol isn't the "tie" symbol
                        {
                            winningSymbol = gameArraySuper[0][i];
                            gameFinished = true;
                            break;
                        }
                    }
                }

            }

            // k-th row has no zeros in it
            if (rowProduct != 0 && !gameFinished) {
                // check that a Nought and a Cross are not together in this column (apart from the tie symbol)
                if (!(rowProduct % 2 == 0 && rowProduct % 3 == 0) && columnProduct != 125) {
                    for (int i = 0; i < 3; i++) {
                        if (gameArraySuper[i][0] != 3) {
                            winningSymbol = gameArraySuper[i][0];
                            gameFinished = true;
                            break;
                        }
                    }
                }

            }
        }

        // check for winner in diagonals if game is unfinished
        if (!gameFinished) {
            int firstDiagonal, secondDiagonal;

            firstDiagonal = primes[gameArraySuper[0][0]] * primes[gameArraySuper[1][1]] * primes[gameArraySuper[2][2]];
            secondDiagonal = primes[gameArraySuper[2][0]] * primes[gameArraySuper[1][1]] * primes[gameArraySuper[0][2]];

            // if first diagonal contains no zeroes
            if (firstDiagonal != 0) {
                // check if noughts and crosses (minus ties) aren't together
                if (!(firstDiagonal % 2 == 0 && firstDiagonal % 3 == 0) && firstDiagonal != 125) {
                    for (int i = 0; i < 3; i++) {
                        if (gameArraySuper[i][i] != 5) {
                            winningSymbol = gameArraySuper[i][i];
                            gameFinished = true;
                            break;
                        }
                    }
                }
            }

            // if second diagonal contains no zeroes
            if (secondDiagonal != 0 && !gameFinished) {
                // check if noughts and crosses (minus ties) aren't together
                if (!(secondDiagonal % 2 == 0 && secondDiagonal % 3 == 0) && secondDiagonal != 125) {
                    for (int i = 0; i < 3; i++) {
                        if (gameArraySuper[2 - i][i] != 5) {
                            winningSymbol = gameArraySuper[2 - i][i];
                            gameFinished = true;
                            break;
                        }
                    }
                }
            }
        }

        // set the winner if there is a winner
        if (gameFinished) {
            if (winningSymbol == 1) {
                this.winner = this.player1.getCurrentSessionID();
            }
            else if (winningSymbol == 2) {
                this.winner = this.player2.getCurrentSessionID();
            }
        }


        return gameFinished;
    }

    public Move getLastMove() {
        return lastMove;
    }

    public String getGameID() {
        return gameID;
    }

    public void setGameID(String gameID) {
        this.gameID = gameID;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public String getGameArray() {
        return new Gson().toJson(gameArray, gameArray.getClass());
    }

    public Player getPlayer(String sessionID) {
        if (player1.getCurrentSessionID().equals(sessionID)) {
            return player1;
        }
        else if (player2.getCurrentSessionID().equals(sessionID)) {
            return player2;
        }
        else {
            return null;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.gameID != null ? this.gameID.hashCode() : 0);
        hash = 89 * hash + (this.player1 != null ? this.player1.hashCode() : 0);
        hash = 89 * hash + (this.player2 != null ? this.player2.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Game other = (Game) obj;
        if ((this.gameID == null) ? (other.gameID != null) : !this.gameID.equals(other.gameID)) {
            return false;
        }
        if ((this.player1 == null) ? (other.player1 != null) : !this.player1.equals(other.player1)) {
            return false;
        }
        if ((this.player2 == null) ? (other.player2 != null) : !this.player2.equals(other.player2)) {
            return false;
        }
        return true;
    }

    public boolean isOpen() {
        if (player2 == null ^ player1 == null) {
            return true;
        }
        else {
            return false;
        }
    }

    public String getCurrentTurn() {
        return currentTurn;
    }

    public int[][][][] getGameArrayCopy() {
        return gameArray.clone();
    }

    public int[][] getGameArraySuperCopy() {
        return gameArraySuper.clone();
    }

    public String getGameType() {
        return gameType;
    }

    public boolean isSinglePlayer() {
        if (!this.gameType.equals("public") && !this.gameType.equals("private")) {
            return true;
        }
        else {
            return false;
        }
    }

    public void deactivate() {
        active = false;
    }

    public boolean isAlive(String gameID) {

        Date timeoutTime = new Date();
        timeoutTime.setTime(timeoutTime.getTime() - 60000);
        if (!isSinglePlayer()) {
            if (!moves.isEmpty()) {
                Date lastMoveTime = moves.get(moves.size() - 1).getTimestamp();
                if (lastMoveTime.before(timeoutTime)) {
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                Date lastMoveTime = joinTimestamp;
                if (lastMoveTime.before(timeoutTime)) {
                    return false;
                }
                else {
                    return true;
                }
            }
        }
        else {
            return true;
        }
    }

    public boolean wonSubgame(int i, int j) {
        return gameArraySuper[i][j] != 0;
    }
}
