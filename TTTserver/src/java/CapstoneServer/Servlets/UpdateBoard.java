/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapstoneServer.Servlets;

import CapstoneServer.GameManager;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**

 @author Farhan
 */
@WebServlet(name = "UpdateBoard", urlPatterns = {"/UpdateBoard/*"})
public class UpdateBoard extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/event-stream");
        response.setCharacterEncoding("UTF-8");
        String gameIDURL = request.getPathInfo().substring(1);

        if (!GameManager.isGameOpen(gameIDURL)) {
            Boolean alive = GameManager.isAlive(gameIDURL);
            PrintWriter out = response.getWriter();
            String output = GameManager.returnJSON(gameIDURL);
            out.append(output);
            out.flush();
            if (!alive) {
                GameManager.deleteGame(gameIDURL);
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Servlet that reserves an SSE connection with the client to update them whenever necessary.";
    }
}
