/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapstoneServer.Servlets;

import AuthServer.CookieUtil;
import AuthServer.Player;
import AuthServer.XMLAuthentication;
import Bots.BotManager;
import CapstoneServer.Game;
import CapstoneServer.GameManager;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

/**

 @author Sam
 */
@WebServlet(name = "Lobby", urlPatterns = {"/Lobby"})
public class Lobby extends HttpServlet {

    private final String cookieName = "sessionID";

    public void newGame(HttpServletRequest request, HttpServletResponse response) {
        String gameID;
        PrintWriter out;
        String sessionID;
        try {
            XMLAuthentication oauth = new XMLAuthentication(request, response);
            out = response.getWriter();
            sessionID = CookieUtil.getCookie(request, cookieName).getValue();
            Player firstPlayer = oauth.getUserDetails(sessionID);
            System.out.println("creating game" + firstPlayer);
            String gameType = request.getParameter("gameType");
            gameID = GameManager.createGame(firstPlayer, gameType);
            if (GameManager.getGame(gameID).isSinglePlayer()) {
                BotManager.loadBot(gameType, gameID);
                String botName = BotManager.getBotName(gameID); //get the name of server or user bot
                Player botPlayer = new Player("(Bot):" + gameType, gameType);
                botPlayer.setNickName(botName);
                GameManager.joinGame(gameID, botPlayer);
                if (GameManager.getGame(gameID).isPlayer1(gameType)) {
                    BotManager.makeBotMove(gameID);
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(gameID);
            out.print(json);

            out.close();
        }
        catch (Exception ex) {
            Logger.getLogger(Lobby.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void quitGame(HttpServletRequest request, HttpServletResponse response) {
        String gameID = request.getParameter("quitID");
        System.out.println("\tClosing game " + gameID);
        GameManager.deleteGame(gameID);
    }

    public void joinGame(HttpServletRequest request, HttpServletResponse response) {
        String gameID;
        PrintWriter out;
        String sessionID;
        try {
            XMLAuthentication oauth = new XMLAuthentication(request, response);
            out = response.getWriter();
            sessionID = CookieUtil.getCookie(request, cookieName).getValue();
            //Grab the players
            gameID = request.getParameter("gameID");
            Player secondPlayer = oauth.getUserDetails(sessionID);
            //Join the game
            GameManager.joinGame(gameID, secondPlayer);
            out.print(gameID);
            //System out some result
            System.out.println("Player has joined the game " + secondPlayer.getFullName());

            out.close();
        }
        catch (Exception ex) {
            Logger.getLogger(Lobby.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void returnOpenGames(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        ArrayList<Game> openGames = GameManager.getOpenGames(null);
        if (CookieUtil.hasCookie(request, cookieName)) {
            try {
                XMLAuthentication oauth = new XMLAuthentication(request, response);
                String playerID = oauth.getUserDetails(CookieUtil.getCookie(request, cookieName).getValue()).getPlayerID();
                openGames = GameManager.getOpenGames(playerID);
            }
            catch (Exception ex) {
                Logger.getLogger(Lobby.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        StringBuilder gameWriter = new StringBuilder();

        // JSONify the game objects
        gameWriter.append("{");
        gameWriter.append("\"game\":[");
        if (!openGames.isEmpty()) {
            for (Game game : openGames) {
                Player p = game.getCreator();
                //ensure the game is not created by "Null's game"
                if (!p.isPlayer() || !p.hasPlayerNames()) {
                    GameManager.deleteGame(game.getGameID());
                    //otherwise list the game...
                }
                else if (CookieUtil.hasCookie(request, cookieName) && !CookieUtil.getCookie(request, cookieName).getValue().equals(p.getCurrentSessionID())) {
                    gameWriter.append("{");
                    gameWriter.append("\t\"gameID\": \"").append(game.getGameID()).append("\",");
                    gameWriter.append("\t\"timestamp\":\"").append(game.getTimestamp()).append("\",");
                    if (p.hasNickName()) {
                        gameWriter.append("\t\"name\": \"").append(p.getNickName()).append("\"");
                    }
                    else {
                        gameWriter.append("\t\"name\": \"").append(p.getFirstName()).append("\"");
                    }
                    gameWriter.append("},");
                }
                else {
                    gameWriter.append("{");
                    gameWriter.append("\t\"gameID\": \"").append(game.getClass()).append("\",");
                    gameWriter.append("\t\"timestamp\": \"").append(game.getTimestamp()).append("\",");
                    if (p.hasNickName()) {
                        gameWriter.append("\t\"name\": \"").append(p.getNickName()).append("\"");
                    }
                    else {
                        gameWriter.append("\t\"name\": \"").append(p.getFirstName()).append("\"");
                    }
                    gameWriter.append("},");
                }
            }
        }
        gameWriter.append("{}");
        gameWriter.append("]");
        gameWriter.append("}");
        out.println(gameWriter.toString());
        out.close();
    }

    private void replayGame(HttpServletRequest request, HttpServletResponse response) {
        String gameID;
        PrintWriter out;
        gameID = request.getParameter("gameID");
        try {
            out = response.getWriter();
            Game game = GameManager.getGame(gameID);
            String json = new Gson().toJson(game.getMoves(), game.getMoves().getClass());
            System.out.println(json);
            out.println(json);
        }
        catch (IOException ex) {
            Logger.getLogger(Lobby.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     Handles the HTTP
     <code>GET</code> method.

     @param request servlet request
     @param response servlet response
     @throws ServletException if a servlet-specific error occurs
     @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        List<String> requestParameterNames = Collections.list((Enumeration<String>) request.getParameterNames());
        if (requestParameterNames.isEmpty()) {
            returnOpenGames(request, response);
        }
        else {
            for (String parameterName : requestParameterNames) {
                if (parameterName.equals("gameID")) {
                    replayGame(request, response);
                }
                else if (parameterName.equals("quitID")) {
                    quitGame(request, response);
                }
            }
        }
    }

    /**
     Handles the HTTP
     <code>POST</code> method.

     @param request servlet request
     @param response servlet response
     @throws ServletException if a servlet-specific error occurs
     @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        List<String> requestParameterNames = Collections.list((Enumeration<String>) request.getParameterNames());

        for (String parameterName : requestParameterNames) {
            if (parameterName.equals("gameType")) {
                System.out.println("\tLobby.java :: Post request to create a new game!");
                newGame(request, response);
            }
            else if (parameterName.equals("gameID")) {
                System.out.println("\tLobby.java :: Post request to joinGame!");
                joinGame(request, response);
            }
            else if (parameterName.equals("quitID")) {
                quitGame(request, response);
            }
        }
    }

    /**
     Returns a short description of the servlet.

     @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
