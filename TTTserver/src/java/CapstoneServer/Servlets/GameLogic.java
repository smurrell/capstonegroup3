/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapstoneServer.Servlets;

import Bots.BotManager;
import CapstoneServer.GameManager;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**

 @author Farhan
 */
@WebServlet(name = "GameLogic", urlPatterns = {"/GameLogic"})
public class GameLogic extends HttpServlet {

    public void addMove(HttpServletRequest request) {
        String sessionID;
        String gameID = request.getParameter("gameID");
        Cookie[] cookies = request.getCookies();
        for (Cookie c : cookies) {
            if (c.getName().equals("sessionID")) {
                sessionID = c.getValue();
                int i = Integer.parseInt(request.getParameter("I"));
                int j = Integer.parseInt(request.getParameter("J"));
                int x = Integer.parseInt(request.getParameter("X"));
                int y = Integer.parseInt(request.getParameter("Y"));

                GameManager.processMove(gameID, sessionID, i, j, x, y);
                
                //call bot
                if (GameManager.getGame(gameID).isSinglePlayer()) {
                    //BotManager.makeBotMove(gameID);
                    BotManager.makeBotMove(gameID);
                }

            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     Handles the HTTP <code>GET</code> method.

     @param request servlet request
     @param response servlet response
     @throws ServletException if a servlet-specific error occurs
     @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     Handles the HTTP <code>POST</code> method.

     @param request servlet request
     @param response servlet response
     @throws ServletException if a servlet-specific error occurs
     @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        addMove(request);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        addMove(request);
    }

    /**
     Returns a short description of the servlet.

     @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet that authenticates moves, and records the games for viewing later.";
    }// </editor-fold>
}
