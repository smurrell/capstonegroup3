/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapstoneServer;

import AuthServer.Player;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**

 @author Farhan
 */
public class GameManager {

    private static Map<String, Game> gameMap = new ConcurrentHashMap<String, Game>();

    public static boolean isAlive(String gameID) {
        if (gameMap.containsKey(gameID)) {
            if (!gameMap.get(gameID).isAlive(gameID)) {
                gameMap.get(gameID).deactivate();
                System.out.println("not Alive " + gameID);
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    }

    public GameManager() {
    }

    public static void clearGames() {
        gameMap.clear();
    }

    public static void addGame(Game game) {
        gameMap.put(game.getGameID(), game);
    }

    public static boolean isGameOpen(String gameID) {
        if (gameMap.get(gameID) != null) {
            return gameMap.get(gameID).isOpen();
        }
        else {
            return false;
        }
    }

    public static String createGame(Player firstPlayer, String gameType) {
        Game game = new Game(firstPlayer, gameType);
        gameMap.put(game.getGameID(), game);
        return game.getGameID();
    }

    public static void joinGame(String gameID, Player secondPlayer) {
        gameMap.get(gameID).joinGame(secondPlayer);
    }

    public static Game getGame(String gameID) {
        return (Game) gameMap.get(gameID);
    }

    public static Boolean hasGame(String gameID) {
        return gameMap.containsKey(gameID);
    }

    public static void deleteGame(String gameID) {
        try {
            if (gameMap.containsKey(gameID)) {
                gameMap.get(gameID).deactivate();
                Thread.sleep(5000);
                gameMap.remove(gameID);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(GameManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static ArrayList<Game> getOpenGames(String playerID) {
        ArrayList<Game> listOpenGames = new ArrayList<Game>();
        for (Map.Entry<String, Game> o : gameMap.entrySet()) {
            String key = o.getKey();
            Game game = o.getValue();
            if (game.isPublicOpen()) {
                if (playerID == null) {
                    listOpenGames.add(game);
                }
                else if (!game.getCreator().getPlayerID().equals(playerID)) {
                    listOpenGames.add(game);
                }
            }
        }

        return listOpenGames;
    }

    /**
     Get a player from an active game, using a gameId

     @param gameID
     @return Player
     */
    // TODO:: might need to write a test for this
    public static boolean processMove(String gameID, String sessionID, int i, int j, int x, int y) {
        Game game = gameMap.get(gameID);

        Player player = game.getPlayer1();
        if (!player.getCurrentSessionID().equals(sessionID)) {
            player = game.getPlayer2();
        }
        String nickName = player.getNickName();
        if (nickName == null) {
            nickName = player.getFirstName();
        }
        Move move = new Move(game.getPlayer(sessionID).getCurrentSessionID(), nickName, i, j, x, y);
        boolean validMove = game.addMove(move);

        return validMove;
    }

    public static boolean isPlayer1(String gameID, String sID) {
        return gameMap.get(gameID).isPlayer1(sID);
    }

    public static String returnJSON(String gameID) {
        return gameMap.get(gameID).returnJSON();
    }

    public static ArrayList<Game> getPastGames(String playerID) {
        ArrayList<Game> pastGames = new ArrayList<Game>();

        // TODO :: Access database to get games
        return pastGames;
    }

    public static int numGames() {
        return gameMap.size();
    }

    public static Boolean vaildMove(String gameID, Move move) {
        return gameMap.get(gameID).validateMove(move);
    }

    public static Boolean wonSubgame(String gameID, int i, int j) {
        return gameMap.get(gameID).wonSubgame(i, j);
    }
}
