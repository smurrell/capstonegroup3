/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Bots;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.objectweb.asm.ClassReader;

/* Different tests:
 * 
 *  - Non-terminating recursion
 *  - Non-terminating loops/long programs (timeout)
 *  - Unsafe APIs (use ASM to inspect dependencies)
 */

/**
 *
 * @author Farhan
 */
public class BotValidator {
    
    private String classOutputFolder = "..\\TTTserver\\CustomBots\\";
    private String packageName = "CustomBots.";
    private String botName;
    private Set<String> dependencies;
    
    private String[] whitelist = {"java.util", "java.lang.Object", "java.util.Random"};
    private ArrayList<String> whitelistAL = new ArrayList<String>(Arrays.asList(whitelist));
    
    public BotValidator(String botName)
    {
        this.botName = botName;
        dependencies = new HashSet<String>();
    }
    
    public boolean isValid() throws IOException
    {
        boolean valid = true;
        
        if (violatesWhiteList())
        {
            System.out.println("\t REJECTED");
            return false;
        }
        
        ExecutorService executor = Executors.newFixedThreadPool(1);
        
        Callable<Boolean> callable = new Callable<Boolean>()
        {
            @Override
            public Boolean call() throws Exception
            {
                BotManager.loadBot(botName);

                int[][][][] gameArray = new int[3][3][3][3];
                int[][] gameArraySuper = new int[3][3];
                int[] lastMove = {1, 1, 1, 1};

                gameArray[1][1][1][1] = -1;     // opponent's move on the first square

                Class params[] = {int[][][][].class, int[][].class, int[].class};
                Object paramsObj[] = {gameArray, gameArraySuper, lastMove};

                int[] output = (int[]) BotManager.runBot(botName, params, paramsObj, "makeMove");
                
                if (output == null)
                {
                    throw new NullPointerException();
                }
                
                return true;
            }
        };
        
        Future<Boolean> future = executor.submit(callable);
        
        try{
            future.get();
        }
        catch (ExecutionException x)
        {
            System.out.println("Execution failed with " + x.getCause());
            valid = false;
        } catch (InterruptedException ex) {
            Logger.getLogger(BotValidator.class.getName()).log(Level.SEVERE, null, ex);
            valid = false;
        }
        

        if (!valid)
        {
            System.out.println("\tREJECTED");
            BotManager.deleteBot(botName);
        }
        else
        {
            System.out.println("\tBOT HAS BEEN ACCEPTED");
        }
        return valid;
    }
    
    
    
    public void runBot() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
    {
        BotManager.loadBot(botName);
        
        int[][][][] gameArray = new int[3][3][3][3];
        int[][] gameArraySuper = new int[3][3];
        int[] lastMove = {1, 1, 1, 1};
        
        gameArray[1][1][1][1] = -1;     // opponent's move on the first square
        
        Class params[] = {int[][][][].class, int[][].class, int[].class};
        Object paramsObj[] = {gameArray, gameArraySuper, lastMove};
        
        BotManager.runBot(botName, params, paramsObj, "makeMove");
    }
    
    public boolean violatesWhiteList() throws IOException
    {
        this.collectDependencies();
        
        
        if (!this.whitelistAL.contains("CustomBots." + botName))
        {
            this.whitelistAL.add("CustomBots." + botName);
        }
        
        for (String dependency : this.dependencies)
        {
            boolean contained = false;
            for (String prefix : this.whitelistAL)
            {
                if (dependency.startsWith(prefix))
                {
                    contained = true;
                    break;
                }
            }
            if (!contained)
            {
                return true;
            }
        }
        
        return false;
    }
    
    public void collectDependencies() throws IOException
    {
         Set<String> classes = new HashSet<String>();
         ClassReader cr = new ClassReader(new FileInputStream(classOutputFolder  + botName + ".class"));
         DependecyCollector visitor = new DependecyCollector();
         
         cr.accept(visitor, 0);
         
         this.dependencies = visitor.getReferenced();
         
         // Debug printing
         for (String s : dependencies)
         {
             System.out.println(s);
         }
    }
}
