/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Bots;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.Locale;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

/**
 *
 * @author Farhan
 */
public class BotCompiler
{
    private static String classOutputFolder = "..\\TTTserver\\CustomBots\\";
    private static String errorMessageJSON = "";
    
    // David
    public static class MyDiagnosticListener implements DiagnosticListener<JavaFileObject>
    {
        @Override
        public void report(Diagnostic<? extends JavaFileObject> diagnostic)
        {
            errorMessageJSON = ""; // clear the error message
            errorMessageJSON += "\"lineNumber\" : \"" + (diagnostic.getLineNumber()-23) + "\", ";
            errorMessageJSON += "\"code\" : \"" + diagnostic.getCode() + "\", ";
            errorMessageJSON += "\"message\" : \"" + diagnostic.getMessage(Locale.ENGLISH).replace('\n', ' ').replace('\\', ' ') + "\", ";
            errorMessageJSON += "\"source\" : \"" + diagnostic.getSource() + "\" ";
        }
    }
    
    // The following class is copied from 
    public static class InMemoryJavaFileObject extends SimpleJavaFileObject
    {
        private String contents = null;
 
        public InMemoryJavaFileObject(String className, String contents) throws Exception
        {
            super(URI.create("string:///" + className.replace('.', '/')
                             + JavaFileObject.Kind.SOURCE.extension), JavaFileObject.Kind.SOURCE);
            this.contents = contents;
        }
 
        @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors)
                throws IOException
        {
            return contents;
        }
    }
    
    private static JavaFileObject getJavaFileObject(String className, String sourceCode)
    {
        JavaFileObject fileObject = null;
        
        try
        {
            fileObject = new InMemoryJavaFileObject(className, sourceCode);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        return fileObject;
    }
    
    public static String getErrorMessageJSON()
    {
        return errorMessageJSON;
    }
    
    public static boolean compile(String directory, String className, String sourceCode)
    {
        JavaFileObject file = getJavaFileObject(className, sourceCode);
        Iterable<? extends JavaFileObject> files = Arrays.asList(file);
        //get system compiler:
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
 
        // for compilation diagnostic message processing on compilation WARNING/ERROR
        MyDiagnosticListener c = new MyDiagnosticListener();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(c, Locale.ENGLISH, null);
        //specify classes output folder
        Iterable<String> options = Arrays.asList("-d", classOutputFolder + "..\\");
        JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager, c, options, null, files);
        
        Boolean result = task.call();
        
        if (result == true)
        {
            System.out.println("Succeeded");
            return true;
        }
        else        // there was a compilation error
        {
            System.out.println("Compilation errors");
            return false;
        }
    }

}
