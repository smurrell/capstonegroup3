/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Bots;

import AuthServer.CookieUtil;
import AuthServer.Player;
import AuthServer.XMLAuthentication;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**

 @author Sam
 */
@WebServlet(name = "BotServlet", urlPatterns = {"/BotServlet"})
public class BotServlet extends HttpServlet {

    private String cookieName = "sessionID";

    private Player getPlayer(HttpServletRequest request, HttpServletResponse response) throws ParserConfigurationException, SAXException, IOException {
        XMLAuthentication oauth = new XMLAuthentication(request, response);
        String sessionID;

        sessionID = CookieUtil.getCookie(request, cookieName).getValue();

        if (sessionID != null) {
            Player client = oauth.getUserDetails(sessionID);
            return client;
        }
        else {
            return null;
        }
    }

    protected void compileBot(HttpServletRequest request, HttpServletResponse response) throws IOException {
        saveBot(request, response);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.write("{ ");

        System.out.println("Trying to compile a bot");

        try {
            String sourceCode = request.getParameter("code");
            String filename = request.getParameter("filename");
            Player client = this.getPlayer(request, response);

            if (sourceCode != null) {
                sourceCode = this.injectCodeForCompilation(sourceCode);
                System.out.println("Compiling " + filename + " on the server!");
                //System.out.println("\n\n" + sourceCode);
                boolean compiled = BotManager.compileBotCode(client, filename, sourceCode);

                if (compiled) {
                    out.write("\"compiled\" : true ,");
                    BotValidator bv = new BotValidator(filename);
                    boolean valid = bv.isValid();

                    if (!valid) {
                        out.write("\"valid\" : false ");
                        deleteClassFile(filename);
                    }
                    else {
                        out.write("\"valid\" : true ");
                    }
                }
                else {
                    String errorMessage = BotCompiler.getErrorMessageJSON();
                    out.write("\"compiled\" : false, ");
                    out.write("\"errorMessage\" : { " + errorMessage + " } ");
                    System.out.println(errorMessage);
                }

            }
            else {
                System.out.println("Couldn't compile!");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        out.write(" }\n\n");
        out.close();
    }

    protected void deleteClassFile(String className) {
        String directory = "..\\TTTserver\\CustomBots\\" + className + ".class";
        File f = new File(directory);

        f.delete();
    }

    protected void saveBot(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("Trying to save a bot");
        try {
            PrintWriter writer = response.getWriter();

            String sourceCode = request.getParameter("code");
            String filename = request.getParameter("filename");

            Player client = this.getPlayer(request, response);

            if (sourceCode == null) // check if any code was sent at all
            {
                System.out.println("No code was sent");
                writer.write("ERROR: No code was sent to the server!");
                writer.close();
            }
            else {
                BotManager.saveBotCode(client, filename, sourceCode);
                //System.out.println(sourceCode); // DEBUG purposes
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void validateBotName(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("Running validateBotName");
        try {
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String botName = request.getParameter("botName");
            File file = new File("..\\TTTserver\\CustomBots");
            String[] names = file.list();

            ArrayList<String> directories = new ArrayList<String>();
            if (names != null) {
                for (String s : names) {
                    if (new File("..\\TTTserver\\CustomBots\\" + s).isDirectory()) {
                        directories.add("..\\TTTserver\\CustomBots\\" + s);
                    }
                }
            }
            boolean valid = true;

            if (botName.equals("")) {
                valid = false;
            }

            if (valid) {
                for (String directory : directories) {
                    File f = new File(directory);

                    String[] sourceFiles = f.list();

                    for (String filename : sourceFiles) {
                        String name[] = filename.replace('.', ',').split(",");
                        System.out.println(name[0]);
                        if (botName.equals(name[0])) {
                            valid = false;
                            break;
                        }
                    }

                    if (!valid) {
                        break;
                    }
                }
            }

            out.write("{ \"valid\" : ");

            if (valid) {
                out.write("true");
            }
            else {
                out.write("false");
            }

            out.write(" }\n\n");

            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected String generateTemplateCode(String botName) {
        String code = "package CustomBots;\n\n"
                + "import java.util.Random;\n\n"
                + "public class " + botName + " {\n"
                + "\t/*\n"
                + "\tThe gameArraySuper is a 2D of length 3 by 3 and indicates\n"
                + "\tsubgame status\n"
                + "\t0 = Unfinished, -1 = Opponent Won, 1 = Bot Won, 3 = Tie\n\n"
                + "\tThe gameArray is a 4D array (3x3x3x3), which represents\n"
                + "\tthe status of al; 81 cells in the game.\n"
                + "\tbot moves have a value of 1, empty cell is 0, opponent is -1\n\n"
                + "\tAddressing for the gameArraySuper\n"
                + "\t\tgameArraySuper[i][j]\n"
                + "\t\ti is the horizontal coordinate and j is the vertical coordinate\n"
                + "\t\twith [0][0] being top left and [2][2] as bottom right\n\n"
                + "\tAddressing for the gameArray is similar to gameArraySuper\n"
                + "\t\tgameArray[i][j][x][y]\n"
                + "\t\ti and j specify which which subgame, while x and y specify which cell in the subgame\n"
                + "\t\texamples:\n"
                + "\t\t\tgameArray[0][0][1][2] is the middle cell bottom row, in the top left subgame\n"
                + "\t\t\tgameArray[2][1][0][0] is the top left cell, in the middle right subgame\n"
                + "\t*/\n"
                
                + "\tpublic int[] makeMove(int[][][][] gameArray, int[][] gameArraySuper, int[] lastMove) {\n"
                + "\t\t//your bot needs to return a int[] of length 4, of where you want to place your move as {i, j, x, y}\n"
                + "\t\tint nextMove[] = new int[4];\n"
                + "\t\tRandom rand = new Random();\n"
                + "\t\t//lastMove contains the coordinates of the last move made, like this {i, j, x, y}\n"
                + "\t\t//so the x and y of the last move is the i and j of your move\n"
                + "\t\tint nextSubGameI = lastMove[2];\n"
                + "\t\tint nextSubGameJ = lastMove[3];\n"
                + "\t\t//but only if the subgame has not been finished\n"
                + "\t\tif (gameArraySuper[nextSubGameI][nextSubGameJ] == 0) {\n"
                + "\t\t\t//it has not been then using the nextSubGameI and nextSubGameJ\n"
                + "\t\t\t//you can not change this bit it is one of the rules\n"
                + "\t\t\tnextMove[0] = nextSubGameI;\n"
                + "\t\t\tnextMove[1] = nextSubGameJ;\n"
                + "\t\t\t//and because our bot is a simple random bot we just pick a random x and y\n"
                + "\t\t\t//***********this bit you can change***********\n"
                + "\t\t\tnextMove[2] = rand.nextInt(3);\n"
                + "\t\t\tnextMove[3] = rand.nextInt(3);\n"
                + "\t\t\t//check if the move is valid, you don't have to but you can\n"
                + "\t\t\twhile (!validateMove(gameArray, gameArraySuper, lastMove, nextMove)) {\n"
                + "\t\t\t\t//if it is not then try another random move\n"
                + "\t\t\t\tnextMove[2] = rand.nextInt(3);\n"
                + "\t\t\t\tnextMove[3] = rand.nextInt(3);\n"
                + "\t\t\t}\n"
                + "\t\t\t//**********************\n"
                + "\t\t}\n"
                + "\t\t//and if the subgame has been won\n"
                + "\t\telse {\n"
                + "\t\t\t//then we just pick random i, j, x, and y\n"
                + "\t\t\t//***********this bit you can change***********\n"
                + "\t\t\tnextMove[0] = nextSubGameI;\n"
                + "\t\t\tnextMove[1] = nextSubGameJ;\n"
                + "\t\t\t\n"
                + "\t\t\tnextMove[2] = rand.nextInt(3);\n"
                + "\t\t\tnextMove[3] = rand.nextInt(3);\n"
                + "\t\t\t\n"
                + "\t\t\t//and again we check if the move is valid\n"
                + "\t\t\twhile (!validateMove(gameArray, gameArraySuper, lastMove, nextMove)) {\n"
                + "\t\t\t\tnextMove[0] = rand.nextInt(3);\n"
                + "\t\t\t\tnextMove[1] = rand.nextInt(3);\n"
                + "\t\t\t\tnextMove[2] = rand.nextInt(3);\n"
                + "\t\t\t\tnextMove[3] = rand.nextInt(3);\n"
                + "\t\t\t}\n"
                + "\t\t\t//**********************\n"
                + "\t\t}\n"
                + "return nextMove;\n"
                + "\t}\n"
                + "}";

        return code;
    }

    protected String injectCodeForCompilation(String sourceCode) {
        // inject the code ater finding the first opening bracket
        int bracketIndex = sourceCode.indexOf('{');
        int sourceLength = sourceCode.length();

        String prefix = sourceCode.substring(0, bracketIndex + 1);
        String suffix = sourceCode.substring(bracketIndex + 1, sourceLength);

        /*
         * Oh god. Please don't ever touch this stuff.
         * 
         */
        String codeToInject = "\n\n"
                + "     public boolean validateMove(int[][][][] gameArray, int[][] gameArraySuper, int[] lastMove, int[] nextMove)\n"
                + "     {\n"
                + "         boolean valid = true;\n"
                + "         int i =nextMove[0], j = nextMove[1], x = nextMove[2], y = nextMove[3];\n"
                + "         int targetI = lastMove[2], targetJ = lastMove[3];\n"
                + "         if (gameArraySuper[targetI][targetJ] == 0) {\n"
                + "             if ((i != targetI | j != targetJ)\n"
                + "                     || gameArray[i][j][x][y] != 0) {\n"
                + "                 valid = false;\n"
                + "             }\n"
                + "         }\n"
                + "         else {\n"
                + "             if ((gameArray[i][j][x][y] != 0) || (i == targetI && j == targetJ)) {\n"
                + "                 valid = false;\n"
                + "             }\n"
                + "         }\n\n\n"
                + "     return valid;\n"
                + "     }\n\n";

        return prefix + codeToInject + suffix;
    }

    protected void saveTemplate(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json");
            PrintWriter out = response.getWriter();

            String botName = request.getParameter("botName");
            Player player = this.getPlayer(request, response);
            BotManager.saveBotCode(player, botName, this.generateTemplateCode(botName));

            File sourcefile = new File("..\\TTTserver\\CustomBots\\" + player.getPlayerID() + "\\" + botName + ".java");

            out.write("{ \"valid\" : ");

            if (sourcefile.exists()) {
                out.write("true");
            }
            else {
                out.write("false");
            }

            out.write("}");

            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void loadCode(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            response.setCharacterEncoding("UTF-8");
            //response.setContentType("application/json");
            out = response.getWriter();

            String path = request.getParameter("path");
            String filepath = "..\\TTTserver\\CustomBots\\" + path + ".java";
            File sourceFile = new File(filepath);
            String sourceCode = "";

            if (sourceFile.exists()) {
                BufferedReader br = new BufferedReader(new FileReader(filepath));
                String line = br.readLine();

                while (line != null) {
                    sourceCode += line + "\n";
                    line = br.readLine();
                }
            }
            else {
                sourceCode = this.generateTemplateCode(path);
            }

            //out.write(" { \"code\" : \"" + sourceCode + "\" } \n\n");
            out.write(sourceCode);
        }
        catch (IOException ex) {
            Logger.getLogger(BotServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            out.close();
        }
    }

    protected void listBots(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            String playerID = this.getPlayer(request, response).getPlayerID();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            out = response.getWriter();

            // aggregate all of the files under that person's ID
            String folderDirectory = "..\\TTTserver\\CustomBots\\" + playerID + "\\";
            File playerDir = new File(folderDirectory);

            if (!playerDir.exists()) {
                out.write("{ \"bot\" : None } \n\n");
                out.close();
                return;
            }

            out.write("{");
            out.write("\"bot\" : [");

            // aggregate all of the saved files
            String[] filenames = playerDir.list();
            for (String file : filenames) {
                String data[] = file.replace(".", ",").split(",");
                out.write("{");
                out.write("\"botName\" : \"" + data[0] + "\", ");
                out.write("\"url\" : \"" + playerID + "/" + data[0] + "\"");
                out.write("}, ");
            }

            out.write("{}");

            out.write("]");
            out.write(" } \n\n");
        }
        catch (Exception ex) {
            Logger.getLogger(BotServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     Handles the HTTP
     <code>GET</code> method.

     @param request servlet request
     @param response servlet response
     @throws ServletException if a servlet-specific error occurs
     @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String method = request.getParameter("method");

        if (method.equals("load")) {
            loadCode(request, response);
        }
        else if (method.equals("list")) {
            listBots(request, response);
        }
    }

    /**
     Handles the HTTP
     <code>POST</code> method.

     @param request servlet request
     @param response servlet response
     @throws ServletException if a servlet-specific error occurs
     @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String method = request.getParameter("method");

        if (method.equals("save")) {
            saveBot(request, response);
        }
        else if (method.equals("compile")) {
            System.out.println("COMPIIIILE");
            compileBot(request, response);
        }
        else if (method.equals("validateName")) {
            System.out.println("VALIDATING NAAAAME");
            validateBotName(request, response);
        }
        else if (method.equals("new")) {
            this.saveTemplate(request, response);
        }
        else {
            System.out.println("Unsupported method");
        }
    }

    /**
     Returns a short description of the servlet.

     @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
