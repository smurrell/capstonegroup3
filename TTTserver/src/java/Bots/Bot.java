/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Bots;

import CapstoneServer.Move;
import java.util.Random;

/**

 @author Sam
 */
public class Bot extends BotTools {

    public Bot(String gameID, String botID) {
        this.gameID = gameID;
        this.botID = botID;
        this.botName = "randomBot";

    }

    public Move makeMove(int[][][][] gameArray, Move lastMove) {
        Move move;
        Random rnd = new Random();
        int subgameI = lastMove.getX();
        int subgameJ = lastMove.getY();

        if (wonSubgame(gameID, subgameI, subgameJ)) {
            subgameI = rnd.nextInt(3);
            subgameJ = rnd.nextInt(3);
        }
        move = new Move(botID, this.botName, subgameI, subgameJ, rnd.nextInt(3), rnd.nextInt(3));

        Boolean vaildMove = vaildMove(gameID, move);
        while (!vaildMove) {
            if (wonSubgame(gameID, subgameI, subgameJ)) {
                subgameI = rnd.nextInt(3);
                subgameJ = rnd.nextInt(3);
            }
            System.out.println("not vaild " + move);
            move = new Move(botID, this.botName, subgameI, subgameJ, rnd.nextInt(3), rnd.nextInt(3));
            System.out.println("bot trying again " + move);
            vaildMove = vaildMove(gameID, move);
        }
        System.out.println("bot making this move " + move);
        return move;
    }
}
