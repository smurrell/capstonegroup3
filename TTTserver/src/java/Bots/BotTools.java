/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Bots;

import CapstoneServer.GameManager;
import CapstoneServer.Move;

/**
 *
 * @author Sam
 */
public class BotTools {

    protected String botID;
    protected String botName;
    protected String gameID;
    
    public static Boolean vaildMove(String gameID, Move move) {
        return GameManager.vaildMove(gameID, move);
    }
    public static Boolean wonSubgame(String gameID, int i, int j) {
        return GameManager.wonSubgame(gameID, i, j);
    }
    
    public String getBotID() {
        return botID;
    }
    
    public void setGameID(String gameID){
        this.gameID = gameID;
    }

    public String getBotName() {
        return botName;
    }

    public void setBotName(String botName) {
        this.botName = botName;
    }
}
