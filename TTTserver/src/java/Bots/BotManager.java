/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Bots;

import AuthServer.Player;
import CapstoneServer.Game;
import CapstoneServer.GameManager;
import CapstoneServer.Move;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**

 @author Farhan
 */
public class BotManager {

    private static Map<String, BotTools> botMap = new ConcurrentHashMap<String, BotTools>();
    private static Map<String, Object> userBots = new ConcurrentHashMap<String, Object>();

    private static String classOutputFolder = "..\\TTTserver\\CustomBots\\";

    public BotManager() {
    }

    /**
     Create the server bot

     @param gameID
     @param gameType (botID)
     @return
     */
    private static String createBot(String gameID, String gameType) {
        Bot bot = new Bot(gameID, gameType);//Change which bot it is here using gameType which is a botID
        botMap.put(bot.getBotID(), bot);
        return bot.getBotID();
    }

    public static Bot getBot(String botID) {
        return (Bot) botMap.get(botID);
    }

    public static String getBotName(String botID) {
        if (botMap.containsKey(botID)) {
            return BotManager.getBot(botID).getBotName();
        }
        return botID;
    }

    /**
     Make the RandomBot Move

     @param gameID
     @return
     */
    private static boolean makeServerBotMove(String gameID) {
        Game game = GameManager.getGame(gameID);
        Bot bot = getBot(game.getGameType()); //get the botID
        System.out.println("botman " + game.getLastMove());
        Move move = bot.makeMove(game.getGameArrayCopy(), game.getLastMove());
        System.out.println("botman " + move);
        boolean validMove = game.addMove(move);
        return validMove;
    }

    // save the code as a source file
    public static void saveBotCode(Player submitter, String programName, String sourceCode) throws IOException {
        String submitterID = submitter.getPlayerID();
        String folderDirectory = classOutputFolder + submitterID;
        String fileDirectory = folderDirectory + "\\" + programName + ".java"; // save as a java file, why not?

        // create the directory if it doesn't exist
        File folder = new File(folderDirectory);
        folder.mkdirs();
        if (!folder.exists()) {
            folder.mkdirs();
        }

        System.out.println(folder.toURI().toURL().toString());
        FileWriter out = new FileWriter(fileDirectory);
        out.write(sourceCode);
        out.close();

        System.out.println("Folder Dir = " + folder.getAbsolutePath());

    }

    public static boolean compileBotCode(Player submitter, String programName, String sourceCode) {
        String submitterID = submitter.getPlayerID();
        String folderDirectory = classOutputFolder + submitterID;
        String className = "CustomBots." + programName;

        return BotCompiler.compile(folderDirectory, className, sourceCode);
    }

    /**
     Check whether this bot has already been loaded in memory.

     @param botName
     @return
     */
    public static boolean isLoaded(String botName) {
        return userBots.containsKey(botName) || botMap.containsKey(botName);
    }

    /**
     Check whether the bot is in the user map or bot map

     @param botName
     @return
     */
    public static boolean isUserBot(String botName) {
        return userBots.containsKey(botName) && !botMap.containsKey(botName);
    }

    public static void deleteBot(String botName) {
        if (userBots.containsKey(botName)) {
            userBots.remove(botName);
        }
    }

    /**
     Load the user bot and throw an Exception

     @param botName
     @throws Exception
     */
    private static void loadUserBot(String botName) throws Exception {
        // load the object only if it doesn't already exist in memory
        if (!userBots.containsKey(botName)) {
            System.out.println("Attemping to Load the bot : " + botName);
            File file = new File(classOutputFolder + "..\\");
            URL url = file.toURI().toURL();
            URL[] urls = {url};
            if (file.exists() && urls.length > 0) {
                System.out.println(urls[0]);
                ClassLoader loader = new URLClassLoader(urls);
                Class thisClass = loader.loadClass("CustomBots." + botName);
                //Only load the class if there is one to load.
                if (thisClass != null) {
                    Object instance = thisClass.newInstance();
                    System.out.println("Class name = " + instance.getClass().getName());
                    userBots.put(botName, instance);
                    System.out.println("Class, " + botName + ", is loaded into memory!");
                }
            }
        }
    }

    /**
     Load the user bot and capture the exception

     @param botName
     */
    public static void loadBot(String botName) {
        try {
            //Load a new Bot
            BotManager.loadUserBot(botName);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     Load a user bot or server bot

     @param botName
     @param gameID
     */
    public static void loadBot(String botName, String gameID) {
        System.out.println("Loading new bot");
        //Always try to load a user bot over the server bot

        try {
            //Load a new Bot
            BotManager.loadUserBot(botName);
        }
        catch (Exception e) {
            //e.printStackTrace();
            System.out.println("User bot load failed " + e.getMessage());
            //If the bot has not been loaded... (Skip Class Exceptions)
            if (!BotManager.isLoaded(botName)) {
                System.out.println("Loading Random Bot For Game");
                BotManager.createBot(gameID, botName);
            }
            else {
                //Update the bots gameID, if it is not a user bot
                BotManager.getBot(botName).setGameID(gameID);
            }
        }

    }

    /**
     Make either the user bot and server bot move.

     @param gameID
     @return
     */
    public static boolean makeBotMove(String gameID) {

        Game game = GameManager.getGame(gameID);
        if (!game.checkForWinner()) {
            String botName = game.getGameType();

            //Make either the user bot or server bot move
            if (BotManager.isUserBot(botName)) {
                return BotManager.runBotForGame(gameID);
            }
            return BotManager.makeServerBotMove(gameID);
        }
        return false;

    }

    /**
     Let this method only be called by makeBotMove

     @param gameID
     @return
     */
    private static boolean runBotForGame(String gameID) {
        Game game = GameManager.getGame(gameID);
        String botName = game.getGameType();    // in a single player game, this will always return the bot's name
        System.out.println(botName);
        int[][][][] gameArray = game.getGameArrayCopy();
        int[][] gameArraySuper = game.getGameArraySuperCopy();
        int[][][][] gameArrayCopy = new int[3][3][3][3];
        int[][] gameArraySuperCopy = new int[3][3];
        Move lastMove = game.getLastMove();
        System.out.println("=======================");
        System.out.println(game.getPlayer1().toJSON());
        System.out.println("+++++++++++++++++++++++");
        boolean isBotPlayer1 = game.isPlayer1(botName);

        // translation map allows the game array that the user bots use to be normalised
        // bot moves will have a value of 1, empty cell is 0, opponent is -1
        int translationMap[] = {0, -1, 1, 3};  // initiliase for the bot being player 2 (doesn't matter if bot isn't)

        if (isBotPlayer1) {
            translationMap = new int[]{0, 1, -1, 3};
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                for (int p = 0; p < 3; p++) {
                    for (int q = 0; q < 3; q++) {
                        int index = gameArray[i][j][p][q];
                        gameArrayCopy[i][j][p][q] = translationMap[index];
                    }
                }
            }
        }

        for (int p = 0; p < 3; p++) {
            for (int q = 0; q < 3; q++) {
                int index = gameArraySuper[p][q];
                gameArraySuperCopy[p][q] = translationMap[index];
            }
        }

        //BotManager.normaliseArrays(gameArrayCopy, gameArraySuperCopy, translationMap);
        int[] lastMoveArray = {lastMove.getI(), lastMove.getJ(), lastMove.getX(), lastMove.getY()};

        Class params[] = {int[][][][].class, int[][].class, int[].class};
        Object paramsObj[] = {gameArrayCopy, gameArraySuperCopy, lastMoveArray};
        try {
            int[] nextMove = (int[]) BotManager.runBot(botName, params, paramsObj, "makeMove");
            System.out.println("User bot, " + botName + ", made move = " + nextMove[0] + ", " + nextMove[1] + ", " + nextMove[2] + ", " + nextMove[3]);

            Move next = new Move("(Bot):" + botName, botName, nextMove[0], nextMove[1], nextMove[2], nextMove[3]);

            return game.addMove(next);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static Object runBot(String botName, Class params[], Object paramsObj[], String methodName) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        System.out.println("RUNNING THE BOT : " + botName);
        Object Output = null;

        if (!userBots.containsKey(botName)) {
            BotManager.loadBot(botName);
        }

        Object instance = userBots.get(botName);
        System.out.println(instance.getClass().getName());
        Method thisMethod = instance.getClass().getDeclaredMethod(methodName, params);

        Output = thisMethod.invoke(instance, paramsObj);
        int move[] = (int[]) Output;
        System.out.println(botName + ", [" + move[0] + ", " + move[1] + ", " + move[2] + ", " + move[3]);

        return Output;
    }
}
