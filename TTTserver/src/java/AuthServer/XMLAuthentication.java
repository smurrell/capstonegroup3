/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AuthServer;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.*;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 This service is designed to only work with OAuth events.
 http://www.ibm.com/developerworks/xml/library/x-xstream/
 http://stackoverflow.com/questions/1211624/how-do-i-encode-utf-8-using-the-xstream-framework
 http://stackoverflow.com/questions/4287071/getting-a-weird-classnotdeffoundexception-on-runtime-with-gwt
 http://xstream.codehaus.org/tutorial.html

 @author Michael Jones 06005586
 */
public class XMLAuthentication extends AuthenticationService implements AuthenticationInterface {

    private String authFileSource;
    public static String authFileName = "../authentication_log.xml";
    private static HashMap<String, Player> authLog = new HashMap<String, Player>();

    /**
     * XMLAuthentication Manager..
     *
     * @param request
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public XMLAuthentication(HttpServletRequest request, HttpServletResponse response) throws ParserConfigurationException, SAXException, IOException {
        //get the file source from the Request manager.......... this will set the path to something like C:\capstonegroup3\TTTserver\build\web\auth_log.xml
        this.authFileSource = request.getServletContext().getRealPath(XMLAuthentication.authFileName);
        // call upon the method from AuthenticationService!
        this.initAuthenication(request, response);

        //Check the Authentication Log requires any changes...
        if (XMLAuthentication.authLog == null || XMLAuthentication.authLog.isEmpty()) {
            String authXMLoutput = this.readAuthLog();
            XMLAuthentication.authLog = XMLAuthentication.XStreamHashMap(authXMLoutput);
        }
    }

    /**
     Generate the authlog by a specified file

     @param xmlFile
     @throws ParserConfigurationException
     @throws SAXException
     @throws IOException
     */
    public XMLAuthentication(File xmlFile) throws ParserConfigurationException, SAXException, IOException {
        String authXMLoutput = XMLAuthentication.readXmlFile(xmlFile);

        //if we have a authentication log
        XMLAuthentication.authLog = XMLAuthentication.XStreamHashMap(authXMLoutput);
    }

    @Override
    public Boolean ExecuteLogin() {

        //get the current player
        Player newPlayer = this.getUserDetails(emailAddress);

        //Don't try to authenticate the user if we do not have an email address...
        if (this.emailAddress != null && !newPlayer.isPlayer()) {

            //increment the player id
            newPlayer.setPlayerId(Integer.toString(getAuthLog().size() + 1));

            //store some information on the player
            newPlayer.setPlayerId(ID);
            newPlayer.setFirstName(firstName);
            newPlayer.setLastName(lastName);
            newPlayer.setEmailAddress(emailAddress);
            newPlayer.setAvatar(avatar);
            this.Authenticate(newPlayer);
        }
        System.out.println("\t" + newPlayer);
        activePlayer = newPlayer;

        //return whether or not we authenticated this user..
        //return auth.hasCookie();
        return true;

    }

    @Override
    public String InternalLogin() {
        //assign a custom message this authentication service...
        this.openCustomMessage("Please provide an email address! ", "danger", "formOptionAlert");
        //log the user into the system...
        if (this.ExecuteLogin()) {
            //update the response...
            this.setRegAuthResponse(activePlayer);
        }
        //push JSON back to the serlvet
        return this.getJSON();
    }

    @Override
    public Player getUserDetails(String currentSessionID) {
        Player nPlayer = new Player();
        if (XMLAuthentication.getAuthLog().containsKey(currentSessionID)) {
            nPlayer = XMLAuthentication.getAuthLog().get(currentSessionID);
        }
        return nPlayer;
    }

    @Override
    public void Authenticate(Player currentPlayer) {

        if (!CookieUtil.hasCookie(request, currentPlayer.getCurrentSessionID())) {
            
            //store the current session id
            currentPlayer.setCurrentSessionID(CookieUtil.createUUID());

            //get the old entry
            Entry<String, Player> authEntry = this.getUserEntryByEmailAddress(emailAddress);

            //remove the old player information before adding the new user
            if (authEntry != null) {
                getAuthLog().remove(authEntry.getKey());
            }

            //store the new player
            XMLAuthentication.getAuthLog().put(currentPlayer.getCurrentSessionID(), currentPlayer);

            //System.out.println("writing to file");
            //write to the hash map
            File authLogFile = new File(this.authFileSource);
            XMLAuthentication.writeHashMap(getAuthLog(), authLogFile);

            //add cookie to server
            this.BeginHandShake(currentPlayer);
        }
    }

    /**
     Update the stored hashMap (Code reuse)

     @param nAuthLog
     */
    public static void writeHashMap(HashMap<String, Player> nAuthLog, File FileSource) {
        FileOutputStream stream = null;
        PrintStream printOut;
        try {
            System.out.println("\t[writeHashMap] Writing to... " + FileSource.getAbsolutePath());
            //prepare the authLog to be stored into the xml..
            XStream xstream = new XStream();
            //stream the file source...
            stream = new FileOutputStream(FileSource.getAbsolutePath());
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Writer writer = new OutputStreamWriter(outputStream, "UTF-8");
            //Open the XML Schema as UTF8
            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
            xstream.toXML(nAuthLog, writer);
            String xml = outputStream.toString("UTF-8");
            printOut = new PrintStream(stream);
            System.out.println("\t[writeHashMap] Closing FileHandlers: Writer and ByteArrayOutputStream");
            //close the writer and output
            writer.close();
            outputStream.close();
            // push the content in
            printOut.print(xml);
            System.out.println("\t[writeHashMap] Closing FileHandlers: PrintStream and FileOutputStream");
            //close the other handlers
            printOut.close();
            stream.close();
        }
        catch (Exception ex) {
            Logger.getLogger(XMLAuthentication.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String LogOut() {
//        this.openCustomMessage("User logged out!", "success", "formOptionAlert");
//
//        //if we are authenticated..
//        if (auth.isAuthenticated()) {
//
//            //get the cookie
//            String currentSessionID = auth.getSessionID();
//            //check and update the log
//            if (getAuthLog().containsKey(currentSessionID)) {
//                getAuthLog().remove(currentSessionID);
//                File authLogFile = new File(this.authFileSource);
//                XMLAuthentication.writeHashMap(getAuthLog(), authLogFile);
//            }
//
//            //log the user out of the system
//            auth.clearAuthentication();
//        }
//        return this.getJSON();
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     Generate file path from the authFileSource property

     @return String
     @throws ParserConfigurationException
     @throws SAXException
     @throws IOException
     */
    private String readAuthLog() throws ParserConfigurationException, SAXException, IOException {
        File fXmlFile = new File(this.authFileSource);
        // Does the file already exist
        if (!fXmlFile.exists()) {
            try {
                fXmlFile.createNewFile();
            }
            catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        //read the file..
        return XMLAuthentication.readXmlFile(fXmlFile);
    }

    /**
     Read an XML File and produce a String output

     @param xmlFile
     @return String
     @throws ParserConfigurationException
     @throws SAXException
     @throws IOException
     */
    public static String readXmlFile(File xmlFile) throws ParserConfigurationException, SAXException, IOException {
        System.out.println("\t[readXmlFile] Reading from... " + xmlFile.getAbsolutePath());
        FileReader file = new FileReader(xmlFile.getAbsolutePath());
        BufferedReader br = new BufferedReader(file);
        if (br.readLine() == null) {
            return null;
        }
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(xmlFile);
        String output;
        try {
            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            output = writer.toString();
            System.out.println("\t[readXmlFile] Closing FileHandler: StringWriter");
            writer.close();
        }
        catch (TransformerException ex) {
            ex.printStackTrace();
            return null;
        }
        finally {
            System.out.println("\t[readXmlFile] Closing FileHandlers: BufferedReader and FileReader");
            br.close();
            file.close();
        }
        return output;
    }

    /**
     Search through the AuthLog by an emailAddress

     @param emailAddress
     @return
     */
    public Entry<String, Player> getUserEntryByEmailAddress(String emailAddress) {
        for (Entry<String, Player> authEntry : getAuthLog().entrySet()) {
            if (authEntry.getValue().getEmailAddress().equals(emailAddress)) {
                return authEntry;
            }
        }
        return null;
    }

    @Override
    public Player getUserByEmail(String emailAddress) {
        Entry<String, Player> authEntry = this.getUserEntryByEmailAddress(emailAddress);
        if (authEntry != null) {
            return authEntry.getValue();
        }
        return new Player();
    }

    /**
     @return the authLog
     */
    public static HashMap<String, Player> getAuthLog() {
        return authLog;
    }

    /**
     @param authLog the authLog to set
     */
    public static void setAuthLog(HashMap<String, Player> authLog) {
        XMLAuthentication.authLog = authLog;
    }

    /**
     @param authXml Set up the AuthLog with a string input
     */
    public static HashMap<String, Player> XStreamHashMap(String authXml) {
        HashMap<String, Player> xstreamMap;
        //if we have a authentication log
        if (authXml != null) {
            XStream xstream = new XStream(new DomDriver());
            @SuppressWarnings("unchecked")
            //initialize a new map from the XStream.fromXML
            HashMap<String, Player> newAuthLog = (HashMap<String, Player>) xstream.fromXML(authXml);
            //pass the map back
            xstreamMap = newAuthLog;
        }
        else {
            xstreamMap = new HashMap<String, Player>();
        }
        return xstreamMap;
    }
}
