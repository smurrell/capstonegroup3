/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AuthServer;

import java.util.UUID;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 Utilities class for Cookies and General Servlet methods.

 @author Michael
 */
public class CookieUtil {

    /**
     Validate if a user has a cookie generated

     @param request
     @return
     */
    public static boolean hasCookie(HttpServletRequest request, final String cKey) {
        Cookie cookie = CookieUtil.getCookie(request, cKey);
        return cookie != null;
    }

    /**
     Get the current HttpSession from HttpServletRequest

     @param request
     @return
     */
//    public static String getHttpSession(final HttpServletRequest request, final String cKey) {
//        HttpSession httpSession = request.getSession();
//        String activeCookie = (String) httpSession.getAttribute(cKey);
//        return activeCookie;
//    }
    /**
     Update the HttpSession with a new cookieVaalue on HttpServletRequest

     @param request
     @param cookieValue
     */
    public static void updateHttpSession(final HttpServletRequest request, final String cKey, final String cValue) {
        HttpSession httpSession = request.getSession();
        System.out.println("updating session...");
        httpSession.setAttribute(cKey, cValue);
    }

    /**
     Clear the HttpSession for Java on HttpServletRequest

     @param request
     */
    public static void clearHttpSession(final HttpServletRequest request) {
        HttpSession httpSession = request.getSession();
        httpSession.invalidate();
    }

    /**
     Returns a cookie given a Key

     @param request
     @param cKey
     @return Cookie or NullException
     */
    public static Cookie getCookie(final HttpServletRequest request, final String cKey) {
        Cookie[] myCookies = request.getCookies();
        if (myCookies != null) {
            for (Cookie nCookie : myCookies) {
                if (nCookie.getName().equals(cKey)) {
                    return nCookie;
                }
            }
        }
        return null;
    }

    /**
     Delete a Cookie from the System

     @src http://www.ehow.com/how_5169279_remove-cookies-java.html
     @param request
     @param cKey
     */
    public static void deleteCookie(final HttpServletRequest request, final String cKey) {
        Cookie activeCookie = CookieUtil.getCookie(request, cKey);
        if (activeCookie != null) {
            activeCookie.setMaxAge(0);
            //response.addCookie(activeCookie);
        }
    }

    /**

     @param request
     @param response
     @param cKey
     @param cValue
     */
    public static void updateOrCreateCookie(final HttpServletRequest request, final HttpServletResponse response, final String cKey, final String cValue) {
        Cookie activeCookie = CookieUtil.getCookie(request, cKey);
        if (activeCookie != null) {
            activeCookie.setValue(cValue);
        }
        else {
            activeCookie = new Cookie(cKey, cValue);
            activeCookie.setMaxAge(60 * 60 * 12);
            response.addCookie(activeCookie);
        }
    }

    /**
     Buffer a hyperlink on the Server

     @param request
     @return String
     */
    public static String getServletHyperLink(final HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        //build a path via the hyperlink to the Folder
        sb.append("http://").append(request.getHeader("host")).append(request.getContextPath()).append('/');
        return sb.toString();
    }

    /**
     Encapsulate the UUID creation in a singlular method

     @return
     */
    public static String createUUID() {
        return UUID.randomUUID().toString();
    }
}
