/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AuthServer;

/**
 String JSON carrier for AuthenticationService and authapi

 @author Michael
 */
public class JsonResponse extends Json {

    private String elementClass;
    private String message;
    private String elementId;
    private Boolean signup = false;
    private String emailAddress;

    /**
     JsonResponse object to store information, that is later converted to String
     JSON object for Javascript Management

     @param message
     @param elementClass
     @param signup
     @param elementId
     */
    public JsonResponse(String message, String elementClass, Boolean signup, String elementId) {
        this.message = message;
        this.elementClass = elementClass;
        this.elementId = elementId;
        this.signup = signup;
    }

    /**
     @return the elementClass
     */
    public String getElementClass() {
        return elementClass;
    }

    /**
     @param elementClass the elementClass to set
     */
    public void setElementClass(String elementClass) {
        this.elementClass = elementClass;
    }

    /**
     @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public void cleanseMessage() {
        if (message != null && !message.isEmpty()) {
            message = message.replace("'", "\\\"");
            message = message.replace("\\", "");
        }
    }

    /**
     @return the signup
     */
    public Boolean getSignup() {
        return signup;
    }

    /**
     @param signup the signup to set
     */
    public void setSignup(Boolean signup) {
        this.signup = signup;
    }

    /**
     @return the element_id
     */
    public String getElementId() {
        return elementId;
    }

    /**
     @param element_id the element_id to set
     */
    public void setElementId(String element_id) {
        this.elementId = element_id;
    }

    @Override
    public String toJSON() {
        this.cleanseMessage();
        return super.toJSON();
    }

    /**
     @return the emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     @param emailAddress the emailAddress to set
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

}
