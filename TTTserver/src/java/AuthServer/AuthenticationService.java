/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AuthServer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 This class simply encapsulates the properties for future classes..

 @author Michael
 */
public class AuthenticationService {

    //Keys that will map information from thie HttpServletRequest to temporary variables
    private final String IDkey = "ID";
    private final String firstNameKey = "firstName";
    private final String lastNameKey = "lastName";
    private final String emailKey = "email";
    private final String avatarKey = "avatar";
    private final String oauthKey = "oauth";
    private final String signupKey = "signup";
    private final String defaultElement = "formOptionAlert";
    private final String cookieName = "sessionID";
    
    //Necessary values for internal authentication (make it accessible by child class)
    public Boolean signup = false;
    public Boolean oauth = false;

    //Cache some information in memory
    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected AuthResponse jresponse;

    //Add the parameters that will mapped by the HttpServletRequest
    protected String ID;
    protected String firstName;
    protected String lastName;
    protected String emailAddress;
    protected String avatar;
    protected Player activePlayer;
    protected String referrer = null;

    /**
     A public method used to initialize the AuthenticationService class

     @param request
     @param response
     */
    public void initAuthenication(HttpServletRequest request, HttpServletResponse response) {

        //set the variables from the constructor parameters
        this.request = request;
        this.response = response;

        //Update the referring url
        this.referrer = request.getHeader("referer");

        //Update the parameters from the paremeter map
        if (this.hasParameter(this.IDkey)) {
            this.ID = this.request.getParameter(this.IDkey);
        }
        if (this.hasParameter(this.firstNameKey)) {
            this.firstName = this.request.getParameter(this.firstNameKey);
        }
        if (this.hasParameter(this.lastNameKey)) {
            this.lastName = this.request.getParameter(this.lastNameKey);
        }
        if (this.hasParameter(this.emailKey)) {
            this.emailAddress = this.request.getParameter(this.emailKey);
        }
        if (this.hasParameter(this.avatarKey)) {
            this.avatar = this.request.getParameter(this.avatarKey);
        }
        if (this.hasParameter(this.signupKey)) {
            String signupValue = this.request.getParameter(this.signupKey);
            this.signup = Boolean.valueOf(signupValue);
        }
        if (this.hasParameter(this.oauthKey)) {
            String oauthValue = this.request.getParameter(this.oauthKey);
            this.oauth = Boolean.valueOf(oauthValue);
        }
    }

    /**
     Get the WebServlet hyperlink

     @return String
     */
    public String getLobbyPath() {
        StringBuilder sb = new StringBuilder();
        sb.append(CookieUtil.getServletHyperLink(request)).append("lobby.html");
        return sb.toString();
    }

    /**
     Decide whether or not we are currently at the lobby

     @return Boolean
     */
    public Boolean atLobby() {
        if (this.referrer != null) {
            //if the user is on the lobby or if they are referring from the base servlet it self.
            return this.referrer.equals(this.getLobbyPath()) || this.referrer.equals(CookieUtil.getServletHyperLink(request));
        }
        return false;
    }

    /**
     Overload the setAuthentication, and set information with the parameters

     @param emailAddress
     @param password
     @param signUp
     @param Oauth
     */
    public void initAuthenication(String ID, String firstName, String lastName, String emailAddress, String avatar, boolean signUp, boolean Oauth) {
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.avatar = avatar;
        this.signup = signUp;
        this.oauth = Oauth;

    }

    /**
     Set a custom message for the user

     @param message
     @param elementClass
     @memory this.signup
     @param elementId
     */
    private void setMessage(String message, String elementClass, String elementId, Boolean SignUp) {
        jresponse = new AuthResponse(message, elementClass, elementId, SignUp, this.atLobby());
        jresponse.setEmailAddress(emailAddress);
    }

    /**
     Set a custom message with the default Element '#formOptionAlert'

     @param message
     @param elementClass
     */
    public void openCustomMessage(String message, String elementClass) {
        this.setMessage(message, elementClass, this.defaultElement, this.signup);
    }

    /**

     @param message
     @param elementClass
     @param elementId
     */
    public void openCustomMessage(String message, String elementClass, String elementId) {
        this.setMessage(message, elementClass, elementId, this.signup);
    }

    /**
     Finish the response for the authapi with "Registered user..
     <emailaddress>" or "Authenticated user.. <emailaddress>"

     @param player
     */
    public void setRegAuthResponse(Player player) {
        if (player != null) {
            //Sam removed redirect, so there is no point in displaying a redirect message.
            //String redirect = this.atLobby() ? "" : "<br/> Redirecting to lobby in <span id='login_timer'>3</span> seconds.";
            jresponse.setMessage(player.buildPlayerName("Welcome back "));
            jresponse.setElementClass("success");
            jresponse.setElementId("formAlert");
        }
    }

    /**
     Return the jresponse as String JSON

     @return
     */
    public String getJSON() {
        return jresponse.toJSON();
    }

    /**
     * Authentication Handshake
     * @param currentPlayer 
     */
    public void BeginHandShake(Player currentPlayer){
        CookieUtil.updateOrCreateCookie(request, response, cookieName, currentPlayer.getCurrentSessionID());
    }
    
    
    /**
     Check the HttpservletRequest has a parameter.

     @param paramKey
     @return
     */
    private Boolean hasParameter(String paramKey) {
        return request.getParameterMap().containsKey(paramKey);
    }
}
