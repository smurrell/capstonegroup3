/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AuthServer;

/**
 * The interface for the Authentication Classes.
 *
 * @author Michael
 */
public interface AuthenticationInterface {

    /**
     * Get player details from an active cookie
     * @param currentSessionID
     * @return
     */
    Player getUserDetails(String currentSessionID);

    /**
     * Log the user in and return JSON
     * @return json
     */
    String InternalLogin();

    /**
     * This method is called from InternalLogin
     * @return
     */
    Boolean ExecuteLogin();

    /**
     * Authenticate the player into the system...
     * @param currentPlayer
     */
    void Authenticate(Player currentPlayer);

    /**
     * Log the user out..
     * @return
     */
    String LogOut();

    /**
     * Get a player by the email address
     * @param emailAddress
     * @return
     */
    Player getUserByEmail(String emailAddress);

}
