/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AuthServer;

import java.sql.Date;

/**

 @author Michael
 */
public class Player extends Json {

    private String playerID;
    private String firstName;
    private String lastName;
    private String currentSessionID;
    private char gameToken;
    private String avatar;
    private String nickName;
    private String emailAddress;
    private Date lastLogOn;
    private Date lastActivityOn;

    /**
     EmailAddress and Session Constructor for the Player

     @param emailAddress
     @param sessionID
     */
    public Player(String playerID, String sessionID) {
        this.playerID = playerID;
        this.currentSessionID = sessionID;
    }

    /**
     Blank constructor
     */
    public Player() {
    }

    /**
     @return whether or not we have any details on this user
     */
    public boolean hasEmailAddress() {
        return emailAddress != null;
    }

    /**
     @return whether or not we have any details on this user
     */
    public boolean hasNickName() {
        return nickName != null;
    }

    /**
     Check if we have a First and Last Name

     @return
     */
    public boolean hasFirstLastName() {
        return this.firstName != null && this.lastName != null;
    }

    /**
     @return Whether or not we can distinguish if a player is here
     */
    public boolean isPlayer() {
        return this.hasEmailAddress() && this.playerID != null && this.currentSessionID != null;
    }

    /**
     Ensure that a player has a name

     @return '
     */
    public boolean hasPlayerNames() {
        return this.getNickName() != null || this.getFirstName() != null;
    }

    /**
     @return the appropriate username to use for this Player
     */
    public String buildPlayerName(String Message) {
        StringBuilder playerName = new StringBuilder();
        playerName.append(Message).append(" ");
        if (this.hasNickName()) {
            playerName.append(nickName);
        }
        else if (this.hasFirstLastName()) {
            playerName.append(this.getFullName());
        }
        else if (this.hasEmailAddress()) {
            playerName.append(this.emailAddress);
        }
        playerName.append("!");
        return playerName.toString();
    }

    /**
     @return the id
     */
    public String getPlayerID() {
        return playerID;
    }

    /**
     @param id the id to set
     */
    public void setPlayerId(String playerID) {
        this.playerID = playerID;
    }

    /**
     @return the avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     @param avatar the avatar to set
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     @return the nickName
     */
    public String getNickName() {
        return nickName;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (this.playerID != null ? this.playerID.hashCode() : 0);
        hash = 41 * hash + (this.currentSessionID != null ? this.currentSessionID.hashCode() : 0);
        hash = 41 * hash + (this.emailAddress != null ? this.emailAddress.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Player other = (Player) obj;
        if ((this.playerID == null) ? (other.playerID != null) : !this.playerID.equals(other.playerID)) {
            return false;
        }
        if ((this.currentSessionID == null) ? (other.currentSessionID != null) : !this.currentSessionID.equals(other.currentSessionID)) {
            return false;
        }
        if ((this.emailAddress == null) ? (other.emailAddress != null) : !this.emailAddress.equals(other.emailAddress)) {
            return false;
        }
        return true;
    }

    /**
     @param nickName the nickName to set
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    /**
     @return the emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     @param emailAddress the emailAddress to set
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     @return the lastLogOn
     */
    public Date getLastLogOn() {
        return lastLogOn;
    }

    /**
     @param lastLogOn the lastLogOn to set
     */
    public void setLastLogOn(Date lastLogOn) {
        this.lastLogOn = lastLogOn;
    }

    /**
     @return the lastActivityOn
     */
    public Date getLastActivityOn() {
        return lastActivityOn;
    }

    /**
     @param lastActivityOn the lastActivityOn to set
     */
    public void setLastActivityOn(Date lastActivityOn) {
        this.lastActivityOn = lastActivityOn;
    }

    /**
     @return the currentSessionID
     */
    public String getCurrentSessionID() {
        return currentSessionID;
    }

    /**
     @param currentSessionID the currentSessionID to set
     */
    public void setCurrentSessionID(String currentSessionID) {
        this.currentSessionID = currentSessionID;
    }

    /**
     @return the gameToken
     */
    public char getGameToken() {
        return gameToken;
    }

    /**
     @param gameToken the gameToken to set
     */
    public void setGameToken(char gameToken) {
        this.gameToken = gameToken;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     Create a realName

     @return
     */
    public String getFullName() {
        StringBuilder realName = new StringBuilder();
        if (this.hasFirstLastName()) {
            realName.append(this.getFirstName().trim()).append(" ").append(this.getLastName().trim());
        }
        return realName.toString();
    }

    public Boolean diffEmail(String nEmailAddress) {
        return !this.emailAddress.equals(nEmailAddress);
    }

    @Override
    public String toString() {
        return "Player{" + "playerID=" + playerID + ", firstName=" + firstName + ", lastName=" + lastName + ", currentSessionID=" + currentSessionID + ", gameToken=" + gameToken + ", avatar=" + avatar + ", nickName=" + nickName + ", emailAddress=" + emailAddress + ", lastLogOn=" + lastLogOn + ", lastActivityOn=" + lastActivityOn + '}';
    }

}
