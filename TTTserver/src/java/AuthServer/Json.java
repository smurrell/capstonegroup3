/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AuthServer;

import com.google.gson.Gson;

/**

 @author Michael
 */
public class Json {

    /**
     Return the active object or 'this' back into Json

     @return
     */
    public String toJSON() {

        Gson gson = new Gson();
        String json = gson.toJson(this);
        return json;
    }

}
