/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapstoneServletsTest;

import AuthServer.Player;
import CapstoneServer.GameManager;
import CapstoneServer.Servlets.UpdateBoard;
import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;
import com.meterware.servletunit.ServletRunner;
import com.meterware.servletunit.ServletUnitClient;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sam
 */
public class UpdateBoardTest {
    
    public UpdateBoardTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void updateBoardJsonTest() {
        try {
            ServletRunner sr = new ServletRunner();
            sr.registerServlet( "UpdateBoard", UpdateBoard.class.getName() );
            ServletUnitClient sc = sr.newClient();
            
            WebRequest request   = new GetMethodWebRequest("http://localhost:8080/TTTserver/UpdateBoard");
            WebConversation wc = new WebConversation();
            WebResponse resp = wc.getResponse(request);
            
            assertTrue(resp.getContentType().equals("application/json"));
        }
        catch (Exception ex) {
            Logger.getLogger(GameLogicTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     @Test
    public void updateBoardCreatedGameTest() {
        try {
            ServletRunner sr = new ServletRunner();
            sr.registerServlet( "UpdateBoard", UpdateBoard.class.getName() );
            ServletUnitClient sc = sr.newClient();
            
            String gameID = GameManager.createGame(new Player("player","seesionID"), "public");
            
            WebRequest request   = new GetMethodWebRequest("http://localhost:8080/TTTserver/UpdateBoard/" + gameID);
            WebConversation wc = new WebConversation();
            WebResponse resp = wc.getResponse(request);
            String response = resp.getResponseMessage();
            
            assertTrue(response.isEmpty());
        }
        catch (Exception ex) {
            Logger.getLogger(GameLogicTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    @Test
    public void updateBoardJoinedGameTest() {
        try {
            ServletRunner sr = new ServletRunner();
            sr.registerServlet( "UpdateBoard", UpdateBoard.class.getName() );
            ServletUnitClient sc = sr.newClient();
            
            String gameID = GameManager.createGame(new Player("player","seesionID"), "public");
            GameManager.joinGame(gameID, new Player("person", "andSessionID"));
            
            WebRequest request   = new GetMethodWebRequest("http://localhost:8080/TTTserver/UpdateBoard/" + gameID);
            WebConversation wc = new WebConversation();
            WebResponse resp = wc.getResponse(request);
            String response = resp.getText();
            
            assertTrue(response.isEmpty());
        }
        catch (Exception ex) {
            Logger.getLogger(GameLogicTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}