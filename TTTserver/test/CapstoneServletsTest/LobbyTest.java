/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapstoneServletsTest;

import AuthServer.Player;
import CapstoneServer.Game;
import CapstoneServer.GameManager;
import CapstoneServer.Servlets.Lobby;
import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;
import com.meterware.servletunit.ServletRunner;
import com.meterware.servletunit.ServletUnitClient;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Farhan
 */
public class LobbyTest {
    
    ServletRunner servletRunner;
    ServletUnitClient client;
    WebConversation webConversation;
    
    public LobbyTest()
    {
        
    }
    
    @Before
    public void setUp()
    {
        servletRunner = new ServletRunner();
        client = servletRunner.newClient();
        webConversation = new WebConversation();
        GameManager.clearGames();
    }
    
    @After
    public void tearDown()
    {
        servletRunner = null;
        client = null;
        webConversation = null;
        GameManager.clearGames();
    }
    
     @Test
    public void lobbyJsonTest() {
        try {
            
            ServletRunner sr = new ServletRunner();
            sr.registerServlet( "Lobby", Lobby.class.getName() );
    
            ServletUnitClient sc = sr.newClient();
            WebRequest request   = new PostMethodWebRequest("http://localhost:8080/TTTserver/Lobby");
            
            WebConversation wc = new WebConversation();
            WebResponse resp = wc.getResponse(request);
            assertTrue(resp.getContentType().equals("application/json"));
        }
        catch (Exception ex) {
            Logger.getLogger(GameLogicTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void testNewGame()
    {
        try {
            
            servletRunner.registerServlet( "Lobby", Lobby.class.getName() );
    
            WebRequest request   = new PostMethodWebRequest("http://localhost:8080/TTTserver/Lobby");
            
            request.setParameter("gameType", "public");
            client.putCookie("sessionID", "testSessionID1");
            
            WebConversation wc = new WebConversation();
            WebResponse resp = webConversation.getResponse(request);
            
            assertNotNull( "No response received", resp );            
        }
        catch (Exception ex) {
            Logger.getLogger(Lobby.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void testJoinGame()
    {
        try {
            GameManager.clearGames();
            Player p = new Player("Smitty Stuff", "1");
            Game game = new Game(p, "public");
            GameManager.addGame(game);
            
            servletRunner.registerServlet( "Lobby", Lobby.class.getName() );
    
            WebRequest request   = new PostMethodWebRequest("http://localhost:8080/TTTserver/Lobby");
            
            request.setParameter("gameID", game.getGameID());
            client.putCookie("sessionID", "testSessionID2");
            
            WebResponse resp = webConversation.getResponse(request);
            
            GameManager.clearGames();
            
            assertNotNull( "No response received", resp );            
        }
        catch (Exception ex) {
            Logger.getLogger(Lobby.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void testReturnOpenGames()
    {
        try {
            Player p1 = new Player("Smitty Stuff", "1");
            Player p2 = new Player("Taffy Luck", "2");
            Game game[] = new Game[10]; // create 10 games on the server, will leave 5 open, and 5 closed
            
            for (int i=0; i<5; i++)
            {
                game[i] = new Game(p1, "public");
                game[i+5] = new Game(p1, "public");
                
                GameManager.addGame(game[i]);
                
                // close the first 5 games by joining them
                GameManager.joinGame(game[i].getGameID(), p2);
                
            }
            
            servletRunner.registerServlet( "Lobby", Lobby.class.getName() );
            
            WebRequest request   = new GetMethodWebRequest("http://localhost:8080/TTTserver/Lobby");
            
            WebResponse resp = webConversation.getResponse(request);
//            assertNotNull("No response received", resp);
            
            String response = resp.getResponseMessage();
            boolean returnedAllOpenGames = true;
            boolean containsSubstring = true;
            
            
            // iterate over all the open games, and check that the game IDs --
            // are substrings of response.
            for (int i=0; i<5; i++) {
                containsSubstring = response.contains(game[i].getGameID());
                
                if (!containsSubstring) {
                    returnedAllOpenGames = false;
                    break;
                }
            }
            
            
            assertTrue(returnedAllOpenGames);
        }
        catch (Exception ex) {
            Logger.getLogger(Lobby.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void testReplayGame()
    {
        try {
            
            servletRunner.registerServlet( "Lobby", Lobby.class.getName() );
    
            WebRequest request   = new GetMethodWebRequest("http://localhost:8080/TTTserver/Lobby");
            
            request.setParameter("gameType", "public");
            client.putCookie("sessionID", "testSessionID1");
            
            WebConversation wc = new WebConversation();
            WebResponse resp = webConversation.getResponse(request);
            
            assertNotNull( "No response received", resp );            
        }
        catch (Exception ex) {
            Logger.getLogger(Lobby.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
