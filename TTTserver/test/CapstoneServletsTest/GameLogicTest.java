/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapstoneServletsTest;

import AuthServer.Player;
import CapstoneServer.Game;
import CapstoneServer.Servlets.GameLogic;
import com.google.gson.Gson;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;
import com.meterware.servletunit.ServletRunner;
import com.meterware.servletunit.ServletUnitClient;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sam
 */
public class GameLogicTest {
    Game game;
   
    @Before
    public void setUp() {
        game = new Game(new Player("onePlayer", "sessionID"), "public");
        game.joinGame(new Player("anotherPlayer", "aDiffernetSessionID"));
    }
    
    @After
    public void tearDown() {
        game = null;
    }
    
    @Test
    public void gameLogicJsonTest() {
        try {
            ServletRunner sr = new ServletRunner();
            sr.registerServlet( "GameLogic", GameLogic.class.getName() );
            ServletUnitClient sc = sr.newClient();
            
            WebRequest request   = new PostMethodWebRequest("http://localhost:8080/TTTserver/GameLogic");
            WebConversation wc = new WebConversation();
            WebResponse resp = wc.getResponse(request);
            
            assertTrue(resp.getContentType().equals("application/json"));
        }
        catch (Exception ex) {
            Logger.getLogger(GameLogicTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void gameLogicAddMoveTest() {
        try {
            
            ServletRunner sr = new ServletRunner();
            sr.registerServlet( "GameLogic", GameLogic.class.getName() );
    
            ServletUnitClient sc = sr.newClient();
            sc.putCookie("sessionID", game.getCurrentTurn());
            WebRequest request   = new PostMethodWebRequest("http://localhost:8080/TTTserver/GameLogic");
            request.setParameter("I", "1");
            request.setParameter("J", "1");
            request.setParameter("X", "1");
            request.setParameter("Y", "1");
            request.setParameter("gameID", game.getGameID());
            
            WebConversation wc = new WebConversation();
            WebResponse resp = wc.getResponse(request);
            
            String json = game.getGameArray();
            int[][][][] gameArray = new Gson().fromJson(json, int[][][][].class);
        
            assertTrue(gameArray[1][1][1][1] == 5 || gameArray[1][1][1][1] == 7);
        }
        catch (Exception ex) {
            Logger.getLogger(GameLogicTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}