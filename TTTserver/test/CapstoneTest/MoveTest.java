/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapstoneTest;

import CapstoneServer.Move;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sam
 */
public class MoveTest {
    
    public MoveTest() {
    }
    @Test
    public void newMoveTest() {
        Move move = new Move("playerID", "nickname", 0,1,2,3);
        assertTrue("fail on playerID", move.getPlayerID().equals("playerID"));
        assertTrue("fail on I", move.getI() == 0);
        assertTrue("fail on J", move.getJ() == 1);
        assertTrue("fail on X", move.getX() == 2);
        assertTrue("fail on Y", move.getY() == 3);
    }            
}