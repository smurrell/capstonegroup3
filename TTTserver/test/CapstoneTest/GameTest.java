/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapstoneTest;

import AuthServer.Player;
import CapstoneServer.Game;
import CapstoneServer.Move;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.UUID;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sam
 */
public class GameTest {
    Player player;
    Game game;

   @Before
    public void setUp() {
        player = new Player();
        player.setCurrentSessionID(UUID.randomUUID().toString());
        player.setPlayerId(UUID.randomUUID().toString());
        game = new Game(player, "public");
    }

    @After
    public void tearDown() {
        player = null;
        game = null;
    }

    @Test
    public void gameIdStructureTest() {
        String[] gameId = game.getGameID().split("-");
        assertTrue(gameId.length == 5);
        assertTrue(gameId[0].length() == 8);
        assertTrue(gameId[1].length() == 4);
        assertTrue(gameId[2].length() == 4);
        assertTrue(gameId[3].length() == 4);
        assertTrue(gameId[4].length() == 12);
    }

    @Test
    public void gameIdUnqiucenessTest() {
        Game game1 = new Game(new Player("thisPlayer","aSessionID"),"public");
        assertFalse(game.getGameID().equals(game1.getGameID()));
    }
    
    @Test
    public void createGameTest() {
        assertTrue(game.getCreator() == player);
    }

    @Test
    public void createPublicTest() {
        assertTrue(game.isPublicOpen());
    }

    @Test
    public void createPrivateTest() {
        Game privateGame = new Game(player, "private");
        assertTrue(!privateGame.isPublicOpen());
    }

    @Test
    public void joinGameTest() {
        Player joiningPlayer = new Player();
        joiningPlayer.setPlayerId(UUID.randomUUID().toString());
        joiningPlayer.setCurrentSessionID(UUID.randomUUID().toString());
        game.joinGame(joiningPlayer);
        assertTrue(joiningPlayer != game.getCreator());
        assertTrue(joiningPlayer == game.getPlayer1() ^ joiningPlayer == game.getPlayer2());
    }

    @Test
    public void gameArrayInitTest() {
        Gson gson = new Gson();
        String json = game.getGameArray();
        int[][][][] gameArray = gson.fromJson(json, int[][][][].class);
        int sum = 0;
        int n=3;
        for (int bRow = 0; bRow < n; bRow++) {
            for (int bCol = 0; bCol < n; bCol++) {
                for (int row = 0; row < n; row++) {
                    for (int col = 0; col < n; col++) {
                        sum = sum + gameArray[bRow][bCol][row][col];
                    }
                }
            }
        }
        assertTrue(sum == 0);
    }
    
    @Test
    public void addMoveMoveListTest() {   //Calls addMove(move) and checks the gameArray
        Player joiningPlayer = new Player();
        joiningPlayer.setPlayerId(UUID.randomUUID().toString());
        joiningPlayer.setCurrentSessionID(UUID.randomUUID().toString());
        game.joinGame(joiningPlayer);
        
        Move move = new Move(joiningPlayer.getPlayerID(), "nickname", 1,1,1,0);
        game.addMove(move);
        ArrayList<Move> moves = game.getMoves();
        
        assertTrue(moves.get(0).equals(move));
    }
    
    @Test
    public void addMoveTest() {   //Calls addMove(move) and checks the gameArray
        Player joiningPlayer = new Player();
        joiningPlayer.setPlayerId(UUID.randomUUID().toString());
        joiningPlayer.setCurrentSessionID(UUID.randomUUID().toString());
        game.joinGame(joiningPlayer);
        
        game.addMove(new Move(joiningPlayer.getPlayerID(), "nickname", 1,1,1,0));
        
        String json = game.getGameArray();
        int[][][][] gameArray = new Gson().fromJson(json, int[][][][].class);
        
        assertTrue(gameArray[1][1][1][0] == 1 || gameArray[1][1][1][0] == 2);
    }
    
    @Test
    public void addBadMoveTest() {   //Calls addMove(move) with a bad move and checks the gameArray
        Player joiningPlayer = new Player();
        joiningPlayer.setPlayerId(UUID.randomUUID().toString());
        joiningPlayer.setCurrentSessionID(UUID.randomUUID().toString());
        game.joinGame(joiningPlayer);
        
        game.addMove(new Move(joiningPlayer.getPlayerID(), "nickname", 0,0,0,1));
        
        String json = game.getGameArray();
        int[][][][] gameArray = new Gson().fromJson(json, int[][][][].class);
        
        assertTrue("gameArray failed", gameArray[0][0][0][1] == 0);
        assertTrue("moveList failed", game.getMoves().isEmpty());
    }
    
    // Game Logic tests - Farhan
    
    @Test
    public void testGetCreator()
    {
        Player p1 = new Player("Smitty Werbenjagermenjensen", "1");
        Player p2 = new Player("Leonard Nimoy", "2");
        Game g = new Game(p1, "public");
        g.joinGame(p2);
        
        assertTrue(p1 == g.getCreator());
    }
    
    // test the game open function
    @Test
    public void testIsOpen()
    {
        Player p1 = new Player("player1", "1");
        Player p2 = new Player("player2", "2");
        
        // g1 will be closed, and g2 will be open
        Game g1 = new Game(p1, "public");
        Game g2 = new Game(p2, "public");
        
        g1.joinGame(p2);
        
        boolean isGame1Open = g1.isOpen();
        boolean isGame2Open = g2.isOpen();
        
        assertTrue(!isGame1Open && isGame2Open);
    }
    
}