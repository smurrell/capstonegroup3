/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapstoneTest;

import AuthServer.Player;
import CapstoneServer.Game;
import CapstoneServer.GameManager;
import CapstoneServer.Move;
import java.util.ArrayList;
//import CapstoneServer.Player;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**

 @author Farhan
 */
public class GameManagerTest {

    Player tempPlayer = new Player("Smitty Werbenjagermenjenson", "12");
    Game tempGame = new Game(tempPlayer, "public");

    public GameManagerTest() {
    }

    @Before
    public void setUp() {
        GameManager.clearGames();
    }

    @After
    public void tearDown() {
        GameManager.clearGames();
    }

    @Test
    public void testAddGames() {
        int numGames = GameManager.numGames();

        GameManager.addGame(tempGame);

        assertTrue(GameManager.numGames() == numGames + 1);
    }

    @Test
    public void testClearGames() {
        GameManager.addGame(tempGame);

        GameManager.clearGames();

        assertTrue(GameManager.numGames() == 0);
    }

    @Test
    public void testIsGameOpen() {
        // create an open game

        GameManager.addGame(tempGame);

        assertTrue(GameManager.isGameOpen(tempGame.getGameID()));
    }

    @Test
    public void testCreateGame() {
        int numGames = GameManager.numGames();

        GameManager.createGame(tempPlayer, "public");

        assertTrue(GameManager.numGames() == numGames + 1);
    }

    @Test
    public void testJoinGame() {
        Player p1 = new Player("p1", "sessionID 1");
        Player p2 = new Player("p2", "sessionID 2");

        Game g = new Game(p1, "public");

        GameManager.addGame(g);
        GameManager.joinGame(g.getGameID(), tempPlayer);

        // game is no longer open if game is joined
        assertTrue(!GameManager.isGameOpen(g.getGameID()));
    }

    @Test
    public void testGetGame() {
        Game g = new Game(tempPlayer, "public");
        GameManager.addGame(g);

        assertTrue(g == GameManager.getGame(g.getGameID()));
    }

    @Test
    public void testDeleteGame() {
        GameManager.clearGames();

        GameManager.addGame(tempGame);
        GameManager.deleteGame(tempGame.getGameID());

        assertTrue(GameManager.getGame(tempGame.getGameID()) == null);
    }

    @Test(timeout = 1000)
    public void CreateGameStressTest() {
        for (int i = 0; i < 500; i++) {
            GameManager.createGame(tempPlayer, "public");
        }
    }

    // create 10 games, leave the first five open, and the last five closed
    @Test
    public void getOpenGames0() {
        Game gameArray[] = new Game[10];

        Player tempPlayer2 = new Player("Murray", "sessionID-Murray");

        for (int i = 0; i < 5; i++) {
            gameArray[i] = new Game(tempPlayer, "public");
            gameArray[i + 5] = new Game(tempPlayer, "public");

            gameArray[i + 5].joinGame(tempPlayer2);
        }

        for (Game game : gameArray) {
            GameManager.addGame(game);
        }

        ArrayList<Game> returnedOpenGames = GameManager.getOpenGames("sessionID-Murray");

        // if it returns more than 5 open games, the test has failed

        assertTrue(returnedOpenGames.size() == 5);

    }

    @Test
    public void getOpenGames1() {
        Game gameArray[] = new Game[10];

        Player tempPlayer2 = new Player("Murray", "sessionID-Murray");

        for (int i = 0; i < 5; i++) {
            gameArray[i] = new Game(tempPlayer, "public");
            gameArray[i + 5] = new Game(tempPlayer, "public");

            gameArray[i + 5].joinGame(tempPlayer2);
        }

        for (Game game : gameArray) {
            GameManager.addGame(game);
        }

        ArrayList<Game> returnedOpenGames = GameManager.getOpenGames("sessionID-Murray");

        for (int i = 0; i < 5; i++) {
            assertTrue(returnedOpenGames.contains(gameArray[i]));
        }
    }

    @Test
    public void replayTest() {
        String gameID = GameManager.createGame(tempPlayer, "public");
        GameManager.joinGame(gameID, new Player("Murray", "sessionID-Murray"));
        
        GameManager.processMove(gameID, GameManager.getGame(gameID).getPlayer1().getCurrentSessionID(), 1, 1, 1, 1);
        
        ArrayList<Move> moves = GameManager.getGame(gameID).getMoves();
        assertTrue(moves.size() == 1);
    }
}
