/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package JunitTests.auth;

import AuthServer.Player;

/**
 * This class extends into the Junit tests for the Player and AuthenticationService
 * It is used to provide properties to each test.
 * @author Michael
 */
public class JUnitTestProperty {
    
    public Player testPlayer = new Player();;
    public String currentSessionID = "currentSessionID";
    public final String ID = "playerID";
    public final String testFirstName = "JunitTest";
    public final String testLastName = "User101";
    public final String testEmailAddress = "capstonegroup3@massey.ac.nz";
    public final String testPicture = null;
    public final String jsonMessage = "JUnit Testing !!!";
    public final String jsonElementClass = "junitClass";
    public final String jsonElementId = "junitDiv";
    public final Boolean signUpParameter = false;
    public final Boolean authParameter = true;
    
}
