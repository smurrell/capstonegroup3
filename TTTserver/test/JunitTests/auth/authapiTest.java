/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package JunitTests.auth;

import AuthServer.Servlets.authapi;
import CapstoneServletsTest.GameLogicTest;
import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;
import com.meterware.servletunit.ServletRunner;
import com.meterware.servletunit.ServletUnitClient;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sam
 */
public class authapiTest {
    
    @Test
    public void authapiJsonTest() {
        try {

            ServletRunner sr = new ServletRunner();
            sr.registerServlet("authapi", authapi.class.getName());

            ServletUnitClient sc = sr.newClient();
            WebRequest request = new GetMethodWebRequest("http://localhost:8080/TTTserver/authapi");
            WebConversation wc = new WebConversation();
            WebResponse resp = wc.getResponse(request);
            assertTrue(resp.getContentType().equals("application/json"));
        }
        catch (Exception ex) {
            Logger.getLogger(GameLogicTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void authapiResponseNotNullTest() {
        try {
            ServletRunner sr = new ServletRunner();
            sr.registerServlet("authapi", authapi.class.getName());
            ServletUnitClient sc = sr.newClient();
            WebRequest request = new GetMethodWebRequest("http://localhost:8080/TTTserver/authapi");
            WebConversation wc = new WebConversation();
            WebResponse resp = wc.getResponse(request);
            assertNotNull(resp);
        }
        catch (Exception ex) {
            Logger.getLogger(GameLogicTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void authapiUsernameReturnTest() {
        try {
            ServletRunner sr = new ServletRunner();
            sr.registerServlet("authapi", authapi.class.getName());
            ServletUnitClient sc = sr.newClient();
           
            WebRequest request = new GetMethodWebRequest("http://localhost:8080/TTTserver/authapi");
            request.setParameter("username", "testUser");
            WebConversation wc = new WebConversation();
            WebResponse resp = wc.getResponse(request);
            String response = resp.getResponseMessage();
            assertTrue(response.contains("Authenticated user..  testUser"));
        }
        catch (Exception ex) {
            Logger.getLogger(GameLogicTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void authapiEmailReturnTest() {
        try {

            ServletRunner sr = new ServletRunner();
            sr.registerServlet("authapi", authapi.class.getName());

            ServletUnitClient sc = sr.newClient();
            WebRequest request = new GetMethodWebRequest("http://localhost:8080/TTTserver/authapi");
            request.setParameter("username", "testUser");
            WebConversation wc = new WebConversation();
            WebResponse resp = wc.getResponse(request);
            String response = resp.getResponseMessage();
            assertTrue(response.contains("\"emailAddress\":\"testUser\""));
        }
        catch (Exception ex) {
            Logger.getLogger(GameLogicTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void authapiSuccessTest() {
        try {
            ServletRunner sr = new ServletRunner();
            sr.registerServlet("authapi", authapi.class.getName());
            ServletUnitClient sc = sr.newClient();
            WebRequest request = new GetMethodWebRequest("http://localhost:8080/TTTserver/authapi");
            request.setParameter("username", "testUser");
            
            WebConversation wc = new WebConversation();
            WebResponse resp = wc.getResponse(request);
            String response = resp.getResponseMessage();
            
            assertTrue(response.contains("success"));
        }
        catch (Exception ex) {
            Logger.getLogger(GameLogicTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}