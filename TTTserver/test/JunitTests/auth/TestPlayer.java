/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package JunitTests.auth;

import AuthServer.CookieUtil;
import AuthServer.Player;
import java.io.IOException;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Michael
 */
public class TestPlayer extends JUnitTestProperty {

    @Before
    public void setUp() throws IOException {
        this.testPlayer.setCurrentSessionID(currentSessionID);
    }

    @Test
    public void testNullPlayer() {
        Player nullPlayer = new Player();
        nullPlayer.setCurrentSessionID(currentSessionID);
        Assert.assertEquals("Test the Null Player", this.testPlayer, nullPlayer);
    }

    @Test
    public void testPlayerHashCode() {
        Player nullPlayer = new Player();
        nullPlayer.setCurrentSessionID(currentSessionID);
        Assert.assertTrue("Test player hashCocde methdod", testPlayer.hashCode() == nullPlayer.hashCode());
    }

    @Test
    public void testDifferentSessionID() {
        String diffSessionId = CookieUtil.createUUID();
        Assert.assertTrue("Check the currentSessionID is not null", this.currentSessionID != null);
        Assert.assertFalse("Test for different Session Ids", this.currentSessionID.equals(diffSessionId));
    }

    @Test
    public void testDifferentPlayer() {
        Player diffPlayer = new Player();
        diffPlayer.setCurrentSessionID("aDifferentSessionID");
        Assert.assertFalse("Test for different players", this.testPlayer.equals(diffPlayer));
    }
}
