/**
 * AppenderLayoutTest Details: This class is testing the velocity engine with
 * other loggers Author: Michael Jones Contact: mikegrahamjones@gmail.com /
 * 06005586 Date: 24-08-2012
 * @package nz.ac.massey.cs.sdc.log4jassignment.s06005586.MemoryAppender
 */
package JunitTests.auth;

import AuthServer.AuthenticationService;
import AuthServer.AuthResponse;

import org.junit.*;
import java.io.IOException;

/**
 * Test the JSON Response from the AuthenticationService
 * Mock the XMLAuthetication Class with EasyMock0
 * http://easymock.org/EasyMock3_1_Documentation.html
 * @author Michael
 */
public class TestAuthenticationService extends JUnitTestProperty {
    
    private AuthenticationService authService;
    
    @Before
    public void setUp() throws IOException {
        
        //setup the AuthenticationService
        this.authService = new AuthenticationService();
        this.authService.initAuthenication(ID, testFirstName, testLastName, this.testEmailAddress, this.testPicture, this.signUpParameter, this.authParameter);
        this.authService.openCustomMessage(this.jsonMessage, this.jsonElementClass, this.jsonElementId);
       
        //setup the Player
        this.testPlayer.setEmailAddress(testEmailAddress);
        this.testPlayer.setCurrentSessionID("aSessionID");
        
    }

    @Test
    public void testCustomJson() {
        AuthResponse nResponse = new AuthResponse(this.jsonMessage, this.jsonElementClass, this.jsonElementId, this.signUpParameter);
        nResponse.setEmailAddress(testEmailAddress);
        //Test the AuthResponse class against the AuthService Class
        Assert.assertTrue(authService.getJSON().equals(nResponse.toJSON()));
    }
    
    @Test
    public void testRegistrationOrSignUpResponse(){
        
        //Declare a Service to test against
        AuthenticationService nAuthService = new AuthenticationService();
        
        //Initialize the AuthenticationService information
        nAuthService.initAuthenication(ID, testFirstName, testLastName, this.testEmailAddress, this.testPicture, this.signUpParameter, this.authParameter);
        nAuthService.openCustomMessage("test", "test");
        
        //setup a Autheticated Response
        nAuthService.setRegAuthResponse(testPlayer);
        authService.setRegAuthResponse(testPlayer);
        
        //Check if they match.
        Assert.assertTrue("The responses match", authService.getJSON().equals(nAuthService.getJSON()));
    }
    
}
