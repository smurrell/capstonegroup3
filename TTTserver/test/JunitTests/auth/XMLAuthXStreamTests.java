/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package JunitTests.auth;

import AuthServer.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import javax.annotation.Resource;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.Before;
import org.xml.sax.SAXException;

/**
 * Authentication Log integrity testing
 *
 * @author Michael
 */
public class XMLAuthXStreamTests {

    @Resource
    private File tempFile = new File("web/" + XMLAuthentication.authFileName);
    private String activeSessionKey = "XMLAuthXStreamTests";
    private XMLAuthentication AuthManager;

    @Before
    public void setUp() throws IOException {
        try {
            AuthManager = new XMLAuthentication(tempFile.getAbsoluteFile());
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XMLAuthXStreamTests.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(XMLAuthXStreamTests.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void testBasicFileCreation() {
        try {
            tempFile.createNewFile();
            assertTrue("File Exists", tempFile.exists());
        } catch (IOException ex) {
            Logger.getLogger(XMLAuthXStreamTests.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void testXStreamRead() {
        try {
            String XmlAuthLog = XMLAuthentication.readXmlFile(tempFile.getAbsoluteFile());
            //Test that the file has content written
            assertTrue("There is consistent information in the AuthLog", XmlAuthLog.length() > 0);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XMLAuthXStreamTests.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(XMLAuthXStreamTests.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLAuthXStreamTests.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void testPlayerAddRemove() {
        try {
            HashMap<String, Player> tempHashMap = XMLAuthentication.getAuthLog();
            //Ensure the player is currently not stored in the HashMap, before we attempt to add and write
            assertFalse("The player does not exist in the tempHashMap HashMap<String, Player>", tempHashMap.containsKey(activeSessionKey));
            Player newPlayer = new Player("demo_user@playtictactoe.to.us", activeSessionKey);
            //Store the player into the HashMap and Write to the file.
            XMLAuthentication.getAuthLog().put(activeSessionKey, newPlayer);
            XMLAuthentication.writeHashMap(XMLAuthentication.getAuthLog(), tempFile.getAbsoluteFile());
            //Read from the HashMap and assert whether or not, we could find the player again.
            String XMLContents = XMLAuthentication.readXmlFile(tempFile.getAbsoluteFile());
            tempHashMap = XMLAuthentication.XStreamHashMap(XMLContents);
            assertTrue("The player exists in the AuthenticationLog HashMap<String, Player>", tempHashMap.containsKey(activeSessionKey));
            //Remove and again write to the HashMap
            XMLAuthentication.getAuthLog().remove(activeSessionKey);
            XMLAuthentication.writeHashMap(XMLAuthentication.getAuthLog(), tempFile.getAbsoluteFile());
            //Cast the String back into a HashMap, and check the contents
            String xmlHashMap = XMLAuthentication.readXmlFile(tempFile.getAbsoluteFile());
            tempHashMap = XMLAuthentication.XStreamHashMap(xmlHashMap);
            assertFalse("The Player was deleted!", tempHashMap.containsKey(activeSessionKey));
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XMLAuthXStreamTests.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(XMLAuthXStreamTests.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLAuthXStreamTests.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void testSessionID2() {
        assertTrue("Check for player testSessionID2", XMLAuthentication.getAuthLog().containsKey("testSessionID2"));
    }

    @Test
    public void testSessionID1() {
        assertTrue("Check for player testSessionID1", XMLAuthentication.getAuthLog().containsKey("testSessionID1"));
    }
}
