var board = [[[[0,0,0],[0,0,0],[0,0,0]],[[0,0,0],[0,0,0],[0,0,0]],[[0,0,0],[0,0,0],[0,0,0]]],[[[0,0,0],[0,0,0],[0,0,0]],[[0,0,0],[0,0,0],[0,0,0]],[[0,0,0],[0,0,0],[0,0,0]]],[[[0,0,0],[0,0,0],[0,0,0]],[[0,0,0],[0,0,0],[0,0,0]],[[0,0,0],[0,0,0],[0,0,0]]]];
var game = [[0,0,0],[0,0,0],[0,0,0]];
var turnCount, turn, lastMove, player, gameID, gameType;
var replay = false;
var resolved = false;

//==============================================================================
// Game functions 
//==============================================================================
function drawTTTTable() {
    document.getElementById('gameDiv').innerHTML="";
    var btable = document.createElement('table');
    btable.setAttribute('class','bboard');
    var btbdy = document.createElement('tbody');
    var gameDiv = document.getElementById('gameDiv');
    if (checkWin()){resolved = true;} // If game over set resolved.
    for (var i = 0; i < board.length; i++) {
        var row = board[i];
        var br = document.createElement('tr');
        br.setAttribute('class', 'row');
        for (var j=0; j< board.length; j++)
            {
            var bd = document.createElement('td');
            var table = document.createElement('table');
            var tbdy = document.createElement('tbody');
            table.setAttribute('class', 'board');
            
            for (var x = 0; x < board.length; x++) {
                var row = board[x];
                var tr = document.createElement('tr');
                tr.setAttribute('class', 'row');

                for (var y = 0; y < row.length; y++) {
                    var coords = [i, j, x, y];
                    var td = document.createElement('td');
                    var img = document.createElement('img');
                    
                    // If it is a replay or not the users turn
                    if(lastMove !== undefined && replay === true || turn !== sID && !resolved){
                        if(game[lastMove[2]][lastMove[3]]!==0&&(i!==lastMove[2]||j!==lastMove[3])&&game[i][j]===0&& resolved === false){
                            img.setAttribute('class', 'cell-opponent-valid');
                        }
                        // If the cell is the latest move.
                        else if (i === lastMove[0] && j === lastMove[1] && x === lastMove[2] && y === lastMove[3] && turnCount !== 0){
                            if (player === 1){ // If is was the crosses move last.
                                img.setAttribute('class', 'cell-prevcross');
                            }
                            if (player === 2){// If is was the noughts move last.
                                img.setAttribute('class', 'cell-prevnought');
                            }
                        }
                        else if (i === lastMove[2] && j === lastMove[3] && turnCount !== 0 && game[i][j]===0 && resolved === false){
                            img.setAttribute('class', 'cell-opponent-valid');
                        }
                        else if (i === 1 && j === 1 && turnCount === 0 && resolved === false){
                            img.setAttribute('class', 'cell-opponent-valid');
                        }
                        else{// It is a normal cell.
                            img.setAttribute('class', 'cell');
                        }
                        
                    }
                    
                    // If not replaying and not first move
                    else if (replay !== true && turnCount !== 0 ){
                        //Change the styling of the last selected cell.
                        if (lastMove !== undefined && i === lastMove[0] && j === lastMove[1] && x === lastMove[2] && y === lastMove[3]){
                            if (board[i][j][x][y] === 1){ // If is was the crosses move last.
                                img.setAttribute('class', 'cell-prevcross');
                            }
                            if (board[i][j][x][y] === 2){// If is was the noughts move last.
                                img.setAttribute('class', 'cell-prevnought');
                            }
                        }
                        
                        //Styling for all other cells
                        else if(lastMove !== undefined && lastMove!==false) {
                            //If the subgame the player was sent to is finished, open all boards for playing in.
                            if(game[lastMove[2]][lastMove[3]]!==0&&(i!==lastMove[2]||j!==lastMove[3])&&game[i][j]===0&& resolved === false){
                                img.setAttribute('class', 'cell-valid');
                                img.setAttribute('onclick', 'cellchoice(' + coords + ')');
                            }
                            //If the cell is in the subgame sent to this turn & that game is not finished. 
                            else if (game[lastMove[2]][lastMove[3]]===0 && i === lastMove[2] && j === lastMove[3]&& resolved === false){
                                img.setAttribute('class', 'cell-valid');
                                img.setAttribute('onclick', 'cellchoice(' + coords + ')');
                            }
                            //Set resolved games to no highlighting
                            else{ 
                                img.setAttribute('class', 'cell');
                            }
                        }
                    }    
                    
                    // If it is the first move.
                    else{
                        if (i === 1 && j === 1){
                            img.setAttribute('class', 'cell-valid');
                            img.setAttribute('onclick', 'cellchoice(' + coords + ')');
                        }
                        else{ img.setAttribute('class', 'cell'); }
                    }
                    
                    // Set images of cells
                    img.setAttribute('id', coords);
                    switch (board[i][j][x][y]) {
                        case 1:
                            img.setAttribute('src', 'styles/images/Cross.jpg');
                            break
                        case 2:
                            img.setAttribute('src', 'styles/images/Nought.jpg');
                            break
                        default:
                            img.setAttribute('src', 'styles/images/Blank.jpg');
                    }
                    td.appendChild(img);
                    tr.appendChild(td);
                }
                tbdy.appendChild(tr);
            }
            table.appendChild(tbdy);
            var boardcoords = [i,j];
            table.setAttribute('id',boardcoords);
            bd.appendChild(table);
            br.appendChild(bd);
        }
        btbdy.appendChild(br);
   }
   btable.appendChild(btbdy);
   gameDiv.appendChild(btable);
}

function gameText() {
    if (checkTie()) {
        if (turn === sID) {
            if (checkWin()) {
                document.getElementById('gameText').innerHTML = "<h2>You lost!</h2>";
                resolved=true;
                //on game end. close the SSE connection
                endGame();		
            }
            else {document.getElementById('gameText').innerHTML = "<h2>It is your turn!</h2>";            }
        }
        else {
            if (checkWin()) {
                document.getElementById('gameText').innerHTML = "<h2>You won!</h2>";
                resolved=true;
                endGame();                
                //writegamestats("writesomin");
            }
            else {document.getElementById('gameText').innerHTML = "<h2>Waiting for other player's move</h2>";}
        }
    }
    else {document.getElementById('gameText').innerHTML = "<h2>The game is a tie!</h2>";resolved=true;}
}

function cellchoice(i,j,x,y) {
    if (board[i][j][x][y] === 0 && checkWin() === false && turn === sID) {
        turn = false;
        var coords = [i, j, x, y];
        var img = document.getElementById(coords);
        board[i][j][x][y] = player;
        $.post("GameLogic", {"sID": player, "gameID": gameID,"I":i, "J":j, "X": x, "Y": y}, function() { });
        if (turnCount % 2 === 0) {
            player = 1;
            img.setAttribute('src', 'styles/images/Cross.jpg');
        }
        if (turnCount % 2 === 1) {
            player = 2;
            img.setAttribute('src', 'styles/images/Nought.jpg');
        }
    }
}

function turnCounter() {
    var counter = 0;
    for (var i = 0; i < board.length; i++) {
        for (var j = 0; j < board.length; j++) {
            for (var x = 0; x < board.length; x++) {
                for (var y = 0; y < board.length; y++) {
                    if (board[i][j][x][y] !== 0) {
                        counter = counter + 1;
                        }
                }
            }
        }
    }
    return counter;
}

//Check each individual table
function checkTableWin(i,j) {
    
    // horizontal win
    // we may be able to find a more efficent search by itterating through tables, check if winner = false else ignore.  
    for (var x = 0; x < 3; x++) {
        if (board[i][j][x][0] === board[i][j][x][1] && board[i][j][x][1] === board[i][j][x][2] && board[i][j][x][0] !== 0) {
            game[i][j]=board[i][j][x][0];
            return true;
        }
    }
    //vertical win
    for (var y = 0; y < 3; y++) {
        if (board[i][j][0][y] === board[i][j][1][y] && board[i][j][1][y] === board[i][j][2][y] && board[i][j][0][y] !== 0) {
            game[i][j]=board[i][j][0][y];
            return true;
        }
    }
    //diagonal win
    if (board[i][j][0][0] === board[i][j][1][1] && board[i][j][1][1] === board[i][j][2][2] && board[i][j][0][0] !== 0) {
        game[i][j]=board[i][j][0][0];
        return true;
    }
    if (board[i][j][0][2] === board[i][j][1][1] && board[i][j][1][1] === board[i][j][2][0] && board[i][j][0][2] !== 0) {
        game[i][j]=board[i][j][0][2];
        return true;
    }
    
    return false;
}

function endGame(){
    //Close the connection to the server as we dont need the updates anymore.
     getConnection();
     connection.close();
     //clear the current table first 
     document.getElementById('gameDiv').innerHTML="";
     //Draw the board once more to clean up highlighting issues
     drawTTTTable();
     makeModal();
     // Wait until the DOM has loaded before querying the document
}

//For future dev. looking into modal stuff.
/*function makeModal(){
    function makeModalStuff(){
        var modal = (function(){
            var	method = {},$overlay,$modal,$content,$close;
            // Center the modal in the viewport
            method.center = function () {
                var top, left;
                top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
		left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;
                $modal.css({
                        top:top + $(window).scrollTop(), 
                        left:left + $(window).scrollLeft()
                });};
            // Open the modal
            method.open = function (settings) {
                $content.empty().append(settings.content);
                $modal.css({
                        width: settings.width || 'auto', 
                        height: settings.height || 'auto'
                });
                method.center();
                $(window).bind('resize.modal', method.center);
                $modal.show();
                $overlay.show();
            };
            // Close the modal
            method.close = function () {
                $modal.hide();
                $overlay.hide();
                $content.empty();
                $(window).unbind('resize.modal');
            };

            // Generate the HTML and add it to the document
            $overlay = $('<div id="overlay"></div>');
            $modal = $('<div id="modal"></div>');
            $content = $('<div id="content"></div>');
            $close = $('<a id="close" href="#">close</a>');
            $modal.hide();
            $overlay.hide();
            $modal.append($content, $close);
            $(document).ready(function(){
                    $('body').append($overlay, $modal);						
            });
            $close.click(function(e){
                    e.preventDefault();
                    method.close();
            });

                return method;
     }());}
      $(document).ready(function(){
         //get the content out of the page.
         var a = $("#gameText").text();
         //Use modal to open the stuff.
         $modal.open({content: a});
     });
}*/

//=================================================================================================================================
//                              More complex game functions
//================================================================================================================================

//Win 3 Subgames to win the game, includes checking for ties as ties are
//Psuedo wins, which accume to win game.
function checkWin(){
    for(i=0; i<3; i++){
            for(j=0;j<3; j++){
                    checkTableNotTie(i,j);
                    checkTableWin(i,j);
                }
            }
    
    /*
     * So when a subgame is won, we set the array at that index to a single char.
     * Valid values are 1,2,3
     */
    //Check for Horizontal wins in each row
    for(var x=0; x<3; x++)
    {
            //If any are not ties, and if they are not all 0s
            if((game[x][0]!==3||game[x][1]!==3||game[x][2]!==3)&&(game[x][0]!==0&&game[x][1]!==0&&game[x][2]!==0)){
                    // First = Second || Tie. Second = Third || Tie. Third = First ||Tie
                    if(((game[x][0]===game[x][1])||(game[x][0]===3))&&((game[x][1]===game[x][2])||(game[x][1]===3)) &&((game[x][2]===game[x][0])||(game[x][2]===3))){
                            return true;
                    }
                    else;
            }
    }
    //Check for Vertical wins in each column
    for(var y=0; y<3; y++)
    {       
            //If any are not ties, and if they are not all 0s
            if((game[0][y]!==3||game[1][y]!==3||game[2][y]!==3)&&(game[0][y]!==0&&game[1][y]!==0&&game[2][y]!==0)){
                // First = Second || Tie. Second = Third || Tie. Third = First ||Tie
                if(((game[0][y]===game[1][y])||(game[0][y]===3))&&((game[1][y]===game[2][y])||game[1][y]===3)&&((game[2][y]===game[0][y])||game[2][y]===3)){
                        return true;
                    }
                    else;
            }
    }

    //Check the 2 Diagonal win cases
    //If any of the cells are not ties and all of the cells are not 0.
    if((game[0][0]!==3||game[1][1]!==3||game[2][2]!==3)&&(game[0][0]!==0&&game[1][1]!==0&&game[2][2]!==0)){
            //First = Second || Tie . Second = Third ||Tie. Third = First ||Tie 
            if((game[0][0]===game[1][1]||game[0][0]===3)&&(game[1][1]===game[2][2]||game[1][1]===3)&&(game[2][2]===game[0][0]||game[2][2]===3)){
                    //If any are not ties, and if they are not all 0s.
                    return true;
            }
    }
    if((game[0][2]!==3||game[1][1]!==3||game[2][0]!==3)&&(game[0][2]!==0&&game[1][1]!==0&&game[2][0]!==0)){
            //First = Second || Tie . Second = Third ||Tie. Third = First ||Tie 
            if((game[0][2]===game[1][1]||game[0][2]===3)&&(game[1][1]===game[2][0]||game[1][1]===3)&&(game[2][0]===game[0][2]||game[2][0]===3)){
                    //If any are not ties, and if they are not all 0s.
                    return true;
            }
    }
    return false;
}

//For third type of wincase we need to impliment a tie value
//Check Sub tables for ties
// Check if no tie.
function checkTableNotTie(i,j) {
    //pass it a board to look at.
    found = [];
    //for (var i = 0; i < 9; i++) {
    //Make a string version of the board.
    var stringb = board.toString();
    //Trim the string of commas, and parenthasies. resulting string is all #'s
    stringb=stringb.replace(/,/g,"");
    //may be a bit fidgity.
    stringb=stringb.substr(((i*9)+(j*27)),9);
    //console.log(i,j,stringb);
    found.push((stringb.indexOf(0) !== -1));
    //If the game is a tie:
    if(found.indexOf(true) === -1)
        {
            game[i][j]=3;
            return true;
        }
    else
        {
            return false;
        }
}

//Check total board for a tie.
function checkTie() {
    found = [];
    //for (var i = 0; i < 9; i++) {
    //Make a string version of the gameboard.
    var stringg = game.toString();
    //If there are still games to play, then the game is not a tie. 
    found.push((stringg.indexOf(0) !== -1));
    return (found.indexOf(true) !== -1);
}

//=================================================================================================================================
//                              Modal Functions
//=================================================================================================================================
