//===============================================================================
//                              Modal Functions
//===============================================================================

//

function initModal() {
    $(document).ready(function() {
        modal = (function() {
            var method = {}, $overlay, $modal, $content, $close;
            method.center = function() {
                var top, left;
                top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
                left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;
                $modal.css({
                    top: top + $(window).scrollTop(),
                    left: left + $(window).scrollLeft()
                });
            };

            method.open = function(settings) {
                $content.empty().append(settings.content);
                $modal.css({
                    width: settings.width || 'auto',
                    height: settings.height || 'auto'
                });

                $overlay.click(close_modal);
                method.center();
                $(window).bind('resize.modal', method.center);
                $modal.show();
                $overlay.show();
            };
            $overlay = $("<div id='lean_overlay'></div>");
            $modal = $('<div id="modal"></div>');
            $content = $('<div id="content"></div>');
            $close = $('<a id="close" class="modal_close" onclick="close_modal();"></a>');

            $modal.hide();
            $overlay.hide();
            $modal.append($content, $close);
            //Append the elements.
            $('body').append($overlay, $modal);
            $close.click(function(e) {
                close_modal();
            });
            return method;
        }());
    });
}

/*
 * Creates the modal dialog for the End game state.
 */
function Modal_End() {
    $(document).ready(function() {
        //Create/get the necessary HTML.
        $wrapper = $('<div id="wrapper"></div>');
        $btnWrapper = $('<div id="btns" style="text-align:center"></div>');
        console.log(gameID);
        $("#gameID").val(gameID);
        $gt = $("#gameText").clone();
        $quit =   $('<input type="submit" value="Quit to Lobby" class="myButton" onclick ="javascript: quitGame(), close_modal(); return false;"/>');
        $replay = $('<input type="submit" value="Replay Game" class="myButton "  name="btnCloseGame"   onclick ="javascript: replayGame();"/>');
        $btnWrapper.append($replay,$quit);
        $wrapper.append($gt, $btnWrapper);
        //Open it.
        modal.open({content: $wrapper});
        //Remove the img overlays from the board.
        game = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
    });
}

/*
 * A Modal open function for bot stuff, however there are some initialization 
 * calls for the bot modal also encapsulated in here for cleanliness.
 */
function Modal_Bot() {
    //Check for login.
    sID = $.cookie('sessionID');
    if (typeof sID === 'undefined' || sID === null) {
        //call modal to prompt user to login.
        modal.open({content: login});
        return false;
    }
    close_modal();
    
    modal.open({content: ModalBot});
    $('#newbotimg').html('');
    $('#validationText').html('');
    $('#nameYourBot').val('');
}

/*
 * A close function, externalized from the init call for convenience and ease
 * of use.
 */
function close_modal() {
    $("#lean_overlay").fadeOut(500);
    $("#modal").fadeOut(500);
}
