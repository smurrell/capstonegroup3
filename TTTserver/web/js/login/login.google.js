/**
@credits:
http://www.netlobo.com/url_query_string_javascript.html
http://www.gethugames.in/blog/2012/04/authentication-and-authorization-for-google-apis-in-javascript-popup-window-tutorial.html
@Google:
https://developers.google.com/accounts/docs/OAuth2UserAgent
**/
var OAUTHURL    =   'https://accounts.google.com/o/oauth2/auth?';
var VALIDURL    =   'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=';
var SCOPE       =   'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email';
var LOGOUT      =   'http://accounts.google.com/Logout';
var TYPE        =   'token';
//declare some variables in Batch to cache..
var acToken, tokenType, expiresIn, user, win, CLIENTID,REDIRECT, TTTserverDev;
//=========Replace these lines with the section below=========
// Check if on release site
if (location.host === "se-projects.massey.ac.nz"){
    
    REDIRECT    =   "http://se-projects.massey.ac.nz/ttt3";
    CLIENTID    =   '913530832002-turr5rkh4br2nrse5dm7m8fiuhiph2r6.apps.googleusercontent.com';
    
}
else if (location.href.indexOf('TTTserver') === -1){
    REDIRECT    =   "http://"+window.location.host+"/";
    CLIENTID    =   '913530832002.apps.googleusercontent.com';
}
else{ // On dev site or localhost
    //TTTserverDev = true
    REDIRECT    =   "http://"+window.location.host+"/TTTserver/";
    if (REDIRECT === "http://localhost:8080/TTTserver/"){
        CLIENTID    =   '913530832002-uhm52qccpvocivi95700fvrmcp041u06.apps.googleusercontent.com';
    }
    else{
        CLIENTID = "913530832002-8s2oq8jiboec3flirri35dpqhdj3avef.apps.googleusercontent.com";
    }
}
/*==============================================================
	CLIENTID    = <your own clientID from google>;
*/

//Build a url to OAuth too..
var _url        =   OAUTHURL + 'scope=' + SCOPE + '&client_id=' + CLIENTID + '&redirect_uri=' + REDIRECT + '&response_type=' + TYPE;
var loggedIn    =   false;

/**
 * Goolge Authentication
 * This doesn't necessary have a requirement to be openned by Click event.
 * But for better user experience, it's recommended.
 */
function googleLogin(){
	console.log("Selected AppId "+CLIENTID);
    win = window.open(_url, "windowname1", 'width=800, height=600');
    var pollTimer   =   window.setInterval(function() {
        if(win === null || typeof(win) === "undefined")
        {
            getPopUpResponse();
        }
        else
        { 
            if (win.closed !== false) { // !== is required for compatibility with Opera
                window.clearInterval(pollTimer);
            }
            try {
                console.log(win.document.URL);
                if (win.document.URL.indexOf(REDIRECT) !== -1) {
                    //Becuase we have a window, fadeOut any options that may appear during the popUpResponse
                    $("#LoginFormOptions").fadeOut('fast');
                    //request the information from Google's OAuth API
                    window.clearInterval(pollTimer);
                    var url =   win.document.URL;
                    acToken =   gup(url, 'access_token');
                    tokenType = gup(url, 'token_type');
                    expiresIn = gup(url, 'expires_in');
                    win.close();
                    //Load some animation while we are processing the user
                    $("#LoginFormOptions").fadeOut('fast', function(){
                        startSonicLoader();
                        validateToken(acToken);
                    });
                }
            //This section is used to catch the permission issued
            //http://stackoverflow.com/questions/7995223/error-permission-denied-to-access-property-document
            } catch(e) {}   
        }
    }, 50);
}

//Validate the user token, after the Google Oauth has been accepted.	
function validateToken(token) {
    $.ajax({
        url: VALIDURL + token,
        data: null,
        success: function(responseText){  
            getUserInfo();
            loggedIn = true;
        },  
        dataType: "jsonp"  
    });
}

// Get User Information from the Authorized Token
function getUserInfo()
{
    $.ajax({
        url: 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' + acToken,
        data: null,
        success: function(resp)
        {
            user = resp;
            console.log(user);
            var avatar = null;
            if (user.picture != null){
                avatar = user.picture;
            }
            
            OAuthSystem(user.id, user.given_name, user.family_name, user.email, avatar);
        },
        dataType: "jsonp"
    });
}

// Retrieve GET protocol parameters from a produced hyperlink
function gup(url, name) {
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\#&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( url );
    if( results === null )
        return "";
    else
        return results[1];
}