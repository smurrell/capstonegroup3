/**
@Author: Michael Jones
@Date: 8/27/2013
@Purpose: External Authentication using Facebook API, Javascript SDK
@Facebook-Resources:
http://stackoverflow.com/questions/5707189/facebook-oauth-retrieve-user-email
http://stackoverflow.com/questions/9847357/facebook-sdk-doesnt-load-all-js-get-failing
**/
var button, userInfo, FacebookAppId;
//=========Replace these lines with the section below=========
//Decide which ClientId from the Facebook OAuth Console to use...
//Go to Localhost
if (REDIRECT === "http://localhost:8080/TTTserver/")
{
    FacebookAppId    =   '301846356624604';
}
//Go to Massey
if (location.host === "se-projects.massey.ac.nz")
{
    FacebookAppId    =   '614212571958303';
}
//Go to Playwithme
else
{
    FacebookAppId    =   '556137064448412';
}
/*==============================================================
	FacebookAppId    =   <your own FacebookAppId>;
*/

/*
 * Initisalize the Facebook Javascript SDK
 */
window.fbAsyncInit = function(){
    FB.init({
        appId: FacebookAppId,
        status: true,
        cookie: true,
        xfbml: true,
    });
};

// Load the SDK asynchronously
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));
/**
 * Facebook Authentication Method.
 * Requirements, must be called within a Click or OnClick Event.
 * @param void
 * @return void
 */
function FacebookLogin()
{
	console.log("Selected AppId "+FacebookAppId);
    //check that a cookie exists
    FB.login(function(response)
    {
        //If the user wishes to login! then begin the animation within leanModal
        $("#LoginFormOptions").fadeOut('fast', function(){
            startSonicLoader();
            
            if(response.authResponse)
            {
                if (response.status === 'connected') {
                    FB.api('/me?fields=name,email', function(info)
                    {
                        //Becuase we have a window, fadeOut any options that may appear during the popUpResponse
                        $("#LoginFormOptions").fadeOut('fast');
                        //Run this user into the /authapi?username=myusername@hostname.TLDR
                        console.log(info);
                        OAuthSystem(info.id, info.name, null, info.email, null);
                    });
                }
            }
            else
            {
                $("#LoginFormOptions").fadeIn('fast');
                $("#SonicLoader").fadeOut('fast', function(){
                    sonicLoader.stop();
                });
            }
        });
    },
    {
        scope:'email'
    });
}