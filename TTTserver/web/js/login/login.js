/*
 * Bind stuff
 */
$(document).ready(function() {
    checkLoginStatus();
});
/*
 * Fade a field in or out
 * @param DOMObject field
 */
function toggleField(field) {
    if (field.hasClass("hidden")) {
        field.removeClass("hidden").css("display", "none").slideDown();
    }
    else {
        field.fadeOut().addClass("hidden");
    }
}
/*
 * Style the fields, use either success or danger based on current style
 * @param Object field
 * @param boolean status
 */
function FieldControl(field, status) {
    if (field.hasClass("warning")) {
        field.removeClass("warning");
    }
    var nextClass = status ? 'success' : 'danger';
    var prevClass = (nextClass === 'success') ? 'danger' : 'success';
    //only remove the class if we have too
    if (field.hasClass(prevClass) && nextClass !== prevClass) {
        field.removeClass(prevClass);
    }
    //add the next class
    field.addClass(nextClass);
}
/*
 * Add the warning style to the field...
 */
function FieldWarning(field, status) {
    if (field.hasClass('success')) {
        field.removeClass('success');
    }
    else if (field.hasClass('danager')) {
        field.removeClass('danger');
    }
    if (typeof(status) !== "undefined" && status) {
        field.addClass('success');
    }
    else {
        field.addClass("warning");
    }
}
/*
 * Display a form alert above the login form
 * @param string message
 * @param string class_var
 * @return void
 */
function displayFormAlert(message, class_var, element_id) {
    var form_alert = $("#" + element_id);
    if (form_alert.hasClass('hidden')) {
        form_alert.removeClass('hidden').hide();
    }
    var other_class = (class_var === 'success') ? 'danger' : 'success';
    if (form_alert.hasClass("warning")) {
        form_alert.removeClass("warning");
    }
    if (form_alert.hasClass(other_class)) {
        form_alert.removeClass(other_class);
    }
    form_alert.addClass(class_var);
    form_alert.html(message);
    form_alert.fadeIn();
}
/**
 * Print a message to the formOptionAlert
 */
function printformOptionAlert(message, class_var) {
    displayFormAlert(message, class_var, "formOptionAlert");
}
//declare a new Sonic Loader
var sonicLoader = new Sonic(loaders[7]);
var signup = false;
/*
 * Start the sonic Loader
 * @return boolean
 */
function startSonicLoader() {
    sonicLoader.play();
    //display a progress Loader
    if (!$("#SonicLoader").doesExist()) {
        $('#LoginForm').append('<div id="SonicLoader" ></div>');
        $('#SonicLoader').append(sonicLoader.canvas);
    }
    else {
        $("#SonicLoader").fadeIn('fast');
    }
}

/*
 * Handdle the login Response
 * @return boolean
 */
function loginPush(postData) {
    $.removeCookie('sessionID');
    $.ajax({
        type: 'POST',
        url: 'authapi',
        data: postData,
        success: function(responseText) {
            if($('#SonicLoader').length){
            $("#SonicLoader").fadeOut('fast', function() {
                //display a message to the user..
                displayFormAlert(responseText['message'], responseText['elementClass'], responseText["elementId"]);
                signup = responseText['signup'];

                //if the user is not being set to authenticate.. and whether or not the formOptions is currently hidden
                //http://stackoverflow.com/questions/759631/jquery-how-to-check-if-the-element-has-certain-css-class-style
                if (responseText['elementClass'] !== 'success' && $("#LoginFormOptions").isHidden()) {
                    $("#LoginFormOptions").fadeIn('fast');
                    if (responseText["elementClass"] === "warning") {
                        //if the server decided we are trying to login..
                        if (signup) {
                            //tell the user to confirm their password..
                            toggleField($("#listConfirmPassword"));
                            FieldWarning($("#listConfirmPassword"));
                        }
                    }
                    //the user has forgotten their password..
                    else {
                        password_field(false);
                    }
                }
                else {
                    //successful login, welcome the user...
                    welcomeUser(postData.firstName);
                        window.setTimeout(function () {
                           close_modal();
                        }, 1000);
                    
                }
            });
            sonicLoader.stop();
        }
        }
    });
}
/*
 * Check whether a site has popups enabled
 * @return boolean
 */
function getPopUpResponse() {
    var pop = window.open("about:blank", "new_window_123", "height=150,width=150");
    // Detect pop blocker
    setTimeout(function() {
        if (!pop || pop.closed || pop.closed === "undefined" || pop === "undefined" || parseInt(pop.innerWidth) === 0 || pop.document.documentElement.clientWidth !== 150 || pop.document.documentElement.clientHeight !== 150) {
            pop && pop.close();
        }
        else {
            pop && pop.close();
            //popups are not enabled..
            $("#SonicLoader").fadeOut('fast', function() {
                displayFormAlert("Popup Blocker is enabled! Please add this site to your exception list.", "danger", "formOptionAlert");
                $("#LoginFormOptions").fadeIn('fast');
                sonicLoader.stop();
            });
        }
    }, 100);
}
/*
 * Handdle the login Response
 * @param integer id
 * @param string given_name
 * @param string family_name
 * @param string email
 * @param string avatar
 * @return void
 */
function OAuthSystem(id, given_name, family_name, email, avatar) {
    //push the information, generate a random password and assign the oauth as true.
    loginPush(createPostData(id, given_name, family_name, email, avatar, true));
}
/**
 * Create some Post Data, based on parameters.
 * @param integer id
 * @param string given_name
 * @param string family_name
 * @param string email
 * @param string avatar
 * @return JSON
 */
function createPostData(id, given_name, family_name, email, avatar, oauth) {
    return {
        'ID' : id,
        'firstName': given_name,
        'lastName' : family_name,
        'email': email,
        'avatar' : avatar,
        'oauth': oauth
    };
}
/*
 * Check that an element exists on the page..
 * @return boolean
 */
$.fn.doesExist = function() {
    return jQuery(this).length > 0;
};
/*
 * Check whether or not an element is hidden
 * @return boolean
 */
$.fn.isHidden = function() {
    return jQuery(this).css("display") === "none";
};
/**
 * Welcome a user to the system, update the navigation item "login".
 * @param void
 */
function welcomeUser(userName) {
    //Generate a random userName if we don't know who logged in
    if(typeof userName == 'undefined' || userName === null){
        userName = "User " + makeid();
    }
    //provide the user with some logout details...
    $("#reviewLogin").html("Logout " + userName);
    $("#reviewLogin").unbind();
    $("#reviewLogin").attr('href', '#');
    $("#reviewLogin").attr('rel', '');
    $("#reviewLogin").attr('onclick', 'logOut()');
}
/**
 * Check if a user is already logged in...
 * @param void
 */
function checkLoginStatus() {
    var activeSID = $.cookie('sessionID');
    appendDummyLogin();
    $.ajax({
        type: 'GET',
        url: 'authapi',
        success: function(responseText) {
            //if the servlet decided we are logged in!
            if (responseText !== null) {
                welcomeUser(responseText.firstName);
            }
            //if we have a active Session with no details assigned to it
            else if (typeof activeSID !== 'undefined' && activeSID !== null) {
                $.removeCookie('sessionID');
                alert("You have signed in at another location, or your session has expired.");
            }
        }
    });
}
/**
 * Log the user out of the system :)
 * @param void
 */
function logOut() {
    //close all open games.
    quitGame();
    $.removeCookie('sessionID');
    //Add the ability to log back in..
    $("#reviewLogin").html("Login");
    $("#reviewLogin").attr('href', '#');
    $("#reviewLogin").attr('onclick', 'modal.open({content: login});');
    $("#LoginFormOptions").css("display", "block");
    $("#formAlert").fadeOut('fast');
    $("#formOptionAlert").fadeOut('fast');
/*
    $.ajax({
        type: 'POST',
        url: 'logout',
        success: function(responseText) {
        // Not implemented yet.
        }
    });
    */
}
/*
 * Provide a response for sites that have popups disabled.
 * @return boolean
 */
function getPopUpResponse() { 
    if($("#SonicLoader").is(":visible")){
        $("#SonicLoader").fadeOut('fast', function() {
            $("#LoginFormOptions").fadeIn('fast');
            sonicLoader.stop();
        });  
    }
    displayFormAlert("Popup Blocker is enabled! Please add this site to your exception list.", "danger", "formOptionAlert");
}
/**
 * Add this method to onclick, within a page.
 * To test leanModal, close events. Without having to pass details.
 * @param void
 */
function LoginAnimationTest(){
    $("#LoginFormOptions").fadeOut('fast', function(){
        startSonicLoader();
        var randomnumber=Math.floor(Math.random()*1000);
        OAuthSystem(randomnumber, makeid(), null, makeEmail(), null);
    });
}
/**
* Append a dummy login to the page 
*/
function appendDummyLogin(){
    if(TTTserverDev){
        var authLogin = $("<a href='#' class='jd' id='sm_jd' title='John Doe Dummy Login' onclick='LoginAnimationTest();' ></a>");
        $('.socialMediaOauth').append(authLogin);
    }
}
/**
 * Create a random Email Address
 */
function makeEmail() { 
    var strValues="abcdefg12345"; 
    var strEmail = ""; 
    var strTmp; 
    for (var i=0;i<10;i++) { 
        strTmp = strValues.charAt(Math.round(strValues.length*Math.random())); 
        strEmail = strEmail + strTmp; 
    } 
    strTmp = ""; 
    strEmail = strEmail + "@"; 
    for (var j=0;j<8;j++) { 
        strTmp = strValues.charAt(Math.round(strValues.length*Math.random())); 
        strEmail = strEmail + strTmp; 
    } 
    strEmail = strEmail + ".com" 
    return strEmail; 
}
/**
 * Create a random ID
 */
function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}