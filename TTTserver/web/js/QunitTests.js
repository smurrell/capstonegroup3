/* 
 Black box tests for server.
 */
// Stop Qunit from reodering tests:
QUnit.config.reorder = false;
// Globals for SIDs
var testSessionID1, testSessionID2

$(document).ready(function() {
    $.ajax({type: 'POST', url: 'authapi', data: createPostData("testPlayer1", "CapstoneTest1", "Blah", "test@tester.com", ""), success: function(data) {
            testSessionID1 = $.cookie("sessionID");
            $.removeCookie("sessionID");
            $.ajax({type: 'POST', url: 'authapi', data: createPostData("testPlayer2", "CapstoneTest1", "Blah", "test@tester.com", ""), success: function(data) {
                testSessionID2 = $.cookie("sessionID");
                $('body').append($("<div id='readyfortests'></div>"));
            }});
        }
    });
    $('#readyfortests').ready(function() {
        console.log("running Test")

    // Helper functions
        function checkNewSPGame(botName) {
            gameID = false;
//            $.cookie("sessionID", testSessionID1);
            createGame(botName);
        }

//        function checkJoinGame() {
//            inGame = false;
//            board = false;
//            turn = null;
//            $.cookie("sessionID", testSessionID2);
//            joinGame(gameID);
//        }

        function playToWin() {
            moves = [1, 1, 2, 0], [2, 0, 1, 1], [1, 1, 1, 1];
            for (var i = 0; i < moves.length - 1; i++) {
                setTimeout(function() {
                    console.log(i);
                    $.removeCookie("sessionID");
                    $.cookie("sessionID", turn);
                    $.post("GameLogic", {
                        "sID": player,
                        "gameID": gameID,
                        "I": moves[i][0],
                        "J": moves[i][1],
                        "X": moves[i][2],
                        "Y": moves[i][3]
                    }, function() {
                    });
                }, 300);

            }
        }

// Tests begin:
        module("Auth Functions");
        test("Basic Authentication", function() {
            //post some basic information to the auth api
            $.removeCookie('sessionID');
            stop();
            setTimeout(function() {
                if(typeof testSessionID1 !== "undefined" && typeof testSessionID2 !== "undefined"){ ;
                    console.log("SessionID1 is " + testSessionID1 +" SessionID2 is "+testSessionID2);
                }
                start();
                ok(testSessionID1 !== null, "Test User 1 was authenticated with ID: " + testSessionID1);
                //ok(testSessionID2 !== null, "Test User 2 was authenticated with ID: " + testSessionID2);
            }, 1000);
        });


        module("Lobby Functions");
        test("Create Single Player Game", function() {
            checkNewSPGame("1");
            stop();
            setTimeout(function() {
                ok(sID !== null, "Session ID not null");
                ok(gameID !== false, "Created game. If failed check test accounts in user database");
                start();
            }, 500);
        });

//        test("Join Game", function() {
//            checkJoinGame();
//            stop();
//            setTimeout(function() {
//                start();
//                ok(sID !== null, "Session ID not null");
//                console.log(board)
//                ok(board !== false, "Board updates begun");
//                ok(turn !== null, "First turn assigned properly. If failed check test accounts in user database");
//                ok(turnCounter() === 0, "Turn is correct");
//            }, 5000);
//        });





        /*test( "Moving on oppents turn", function() {
         if (turn === "sessionID1"){
         $.cookie("sessionID", "sessionID2");
         }
         if (turn === "sessionID1"){
         $.cookie("sessionID", "sessionID2");
         }
         $.post("GameLogic", {"sID": player, "gameID": gameID,"I":1, "J":1, "X": 1, "Y": 2}, function() {});
         stop();
         setTimeout(function() {
         ok(board[1][1][1][2] === 0, "Server did not modify cell value");
         ok(turnCounter() === 0, "Turn count updated");
         start();
         }, 3000);
         });*/

        test("Invalid Cell Choice", function() {
            $.cookie("sessionID", turn);
            $.post("GameLogic", {
                "sID": player,
                "gameID": gameID,
                "I": 0,
                "J": 0,
                "X": 0,
                "Y": 0
            }, function() {
            });
            stop();
            setTimeout(function() {
                start();
                ok(board[0][0][0][0] === 0, "Server did not modify cell value");
                ok(turnCounter() === 0, "Turn count updated");
            }, 3000);

        });



//        test("Legal Move", function() {
////            $.removeCookie("sessionID");
////            $.cookie("sessionID", turn);
//            console.log(lastMove);
//            $.post("GameLogic", {
//                "sID": player,
//                "gameID": gameID,
//                "I": 1,
//                "J": 1,
//                "X": 0,
//                "Y": 2
//            }, function() {
//            });
//            stop();
//            setTimeout(function() {
//
//                ok(board[1][1][0][2]) !== 0, "Move updated";
//                ok(turnCounter() === 1, "Turn count updated");
//                start();
//            }, 3000);
//        });
    })

});

/*
 test( "Legal Move 2", function() {
 $.post("GameLogic", {
 "sID": player, 
 "gameID": gameID,
 "I":0, 
 "J":2, 
 "X": 1, 
 "Y": 1
 }, function() {});
 stop();
 setTimeout(function() {
 $.removeCookie("sessionID");
 $.cookie("sessionID", turn);
 ok(board[0][2][1][1]) !== 0, "Move updated";
 ok(turnCounter() === 2, "Turn count updated");
 start();
 }, 3000);
 });
 
 test( "Legal Move 3", function() {
 $.post("GameLogic", {
 "sID": player, 
 "gameID": gameID,
 "I":1, 
 "J":1, 
 "X": 2, 
 "Y": 0
 }, function() {});
 stop();
 setTimeout(function() {
 $.removeCookie("sessionID");
 $.cookie("sessionID", turn);
 ok(board[1][1][2][0]) !== 0, "Move updated";
 ok(turnCounter() === 3, "Turn count updated");
 start();
 }, 3000);
 });
 
 test( "Legal Move 4", function() {
 $.post("GameLogic", {
 "sID": player, 
 "gameID": gameID,
 "I":2, 
 "J":0, 
 "X": 1, 
 "Y": 1
 }, function() {});
 stop();
 setTimeout(function() {
 $.removeCookie("sessionID");
 $.cookie("sessionID", turn);
 ok(board[2][0][1][1]) !== 0, "Move updated";
 ok(turnCounter() === 4, "Turn count updated");
 start();
 }, 3000);
 });
 
 test( "Legal Move 5", function() {
 $.post("GameLogic", {
 "sID": player, 
 "gameID": gameID,
 "I":1, 
 "J":1, 
 "X": 1, 
 "Y": 1
 }, function() {});
 stop();
 setTimeout(function() {
 $.removeCookie("sessionID");
 $.cookie("sessionID", turn);
 ok(board[1][1][1][1]) !== 0, "Move updated";
 ok(turnCounter() === 5, "Turn count updated");
 console.log(board[1][1][0], board[1][1][1], board[1][1][2], checkWin())
 ok(checkWin(), "Game was won")
 start();
 }, 3000);
 });
 
 
 
 
 
 test( "Play game to win", function() {
 playToWin()
 stop();
 setTimeout(function() {
 console.log (turnCounter())
 ok(turnCounter() === 5, "All moves accepted");
 start();
 }, 20000);
 });
 */

// Refresh gameList
// Assert fail when ingame for create join replay
// board from game == board for replay

// Correct modification of game board on cellchoice
// Correct wincase and tie false and true
// Check last move

// set gamearray to various states and immediately test 
// outcome

//Cellchoice without logging in

// Check array structure