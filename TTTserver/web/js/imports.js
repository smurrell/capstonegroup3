document.write('<meta charset="UTF-8" />');
document.write('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');

document.write('<link rel="stylesheet" href="styles/style.css">');
document.write('<link rel="shortcut icon" href="styles/images/favicon.ico"/>');

document.write('<!-- Library Scripts -->');
document.write('<script type="text/javascript" src="js/jquery.min.js"></script>');
document.write('<script type="text/javascript" src="js/jquery.cookie.js"></script>');
document.write('<script type="text/javascript" src="js/login/sonic.js"></script>');
document.write('<script type="text/javascript" src="js/login/sonic.loaders.js"></script>');
document.write('<script type="text/javascript" src="js/login/hmac-sha256.js"></script>');

document.write('<!-- Capstone Scripts -->');
document.write('<script type="text/javascript" src="js/gameLogic.js"></script>');
document.write('<script type="text/javascript" src="js/gameRendering.js"></script>');
document.write('<script type="text/javascript" src="js/Modal.js"></script>');
document.write('<script type="text/javascript" src="js/lobby.js"></script>');
document.write('<script type="text/javascript" src="js/IDE.js"></script>');
document.write('<script type="text/javascript" src="js/login/login.js"></script>');
document.write('<script type="text/javascript" src="js/login/login.google.js"></script>');
document.write('<script type="text/javascript" src="js/login/login.facebook.js"></script>');


document.write('<!-- Tour Scripts -->');
document.write('<link rel="stylesheet" type="text/css" href="js/tour/introtour-ui.css">');
document.write('<link rel="stylesheet" type="text/css" href="js/tour/introtour-buttons.css">');
document.write('<script src="js/tour/introtour.min.js"></script>');
document.write('<script src="js/tour/tour.js"></script>');

//VARS for holding modal contents
var html, welcome, login, ModalBot;