//==============================================================================
//                    Game Board Rendering Function
//==============================================================================
function drawTTTTable() {
    $('#gameDiv').empty();
    $('#gameWrap').show();
    var btable = document.createElement('table');
    btable.className = 'bboard';
    var btbdy = document.createElement('tbody');
    var gameDiv = document.getElementById('gameDiv');
    if (checkWin()){resolved = true;}
    for (var i = 0; i < board.length; i++) {
        var row = board[i];
        var br = document.createElement('tr');
        br.setAttribute('class', 'row');
        for (var j=0; j< board.length; j++) {
            var boardcoords = [i,j];
            var bd = document.createElement('td');
            var tableWrap = document.createElement('div');
            tableWrap.className = 'tableWrap';
            var tableImg = document.createElement('img');
            tableImg.className = 'hiddenImg';
            // Big picture overlay
            if (game[i][j] !== 0){
                tableImg.className = 'tableImg';
                if (game[i][j] === 1){
                    tableImg.setAttribute('src', 'styles/images/Cross.svg');
                }
                if (game[i][j] === 2){
                    tableImg.setAttribute('src', 'styles/images/Nought.svg');
                }
                if (game[i][j] === 3){
                    tableImg.setAttribute('src', 'styles/images/tie.svg');
                }
            }
            var table = document.createElement('table');
            table.className = 'board';
            table.setAttribute('id',boardcoords);
            var tbdy = document.createElement('tbody');
            for (var x = 0; x < board.length; x++) {
                var row = board[x];
                var tr = document.createElement('tr');
                tr.className = 'row';
                for (var y = 0; y < row.length; y++) {
                    var coords = [i, j, x, y];
                    var td = document.createElement('td');
                    var img = document.createElement('img');
                    // Reset/set img class
                    td.className = 'cell';
                    // If it is a replay or not the users turn
                    if(lastMove !== undefined && (replay === true || turn !== sID) && !resolved){
                        if(!replay){
                            if(game[lastMove[2]][lastMove[3]]!==0&&(i!==lastMove[2]||j!==lastMove[3])&&game[i][j]===0){
                                td.className += (' opponentValid');
                            }
                            // If the cell is the latest move.
                            else if (i === lastMove[0] && j === lastMove[1] && x === lastMove[2] && y === lastMove[3] && turnCount !== 0){
                                    img.className += (' animated flash');
                                }
                            else if (i === lastMove[2] && j === lastMove[3] && turnCount !== 0 && game[i][j]===0){
                                td.className += (' opponentValid');
                            }
                            else if (i === 1 && j === 1 && turnCount === 0){
                                td.className += (' opponentValid');
                            }
                       }
                       //Replay Rendering
                       else{
                            // If the cell is the latest move.
                            if (i === lastMove[0] && j === lastMove[1] && x === lastMove[2] && y === lastMove[3] && turnCount !== 0){
                                    img.className += (' animated flash');
                                }
                            //Opponent
                            
                                //If the subgame the player was sent to is finished, open all boards for playing in.
                                if(game[lastMove[2]][lastMove[3]]!==0&&(i!==lastMove[2]||j!==lastMove[3])&&game[i][j]===0){
                                    td.className += (' opponentValid');
                                }
                                //If the cell is in the subgame sent to this turn & that game is not finished. 
                                else if (game[lastMove[2]][lastMove[3]]===0 && i === lastMove[2] && j === lastMove[3]){
                                    td.className += (' opponentValid');
                                }
                            }
                    }
                    // If not replaying and not first move
                    else if (replay !== true && turnCount !== 0 && lastMove !== undefined){
                        if(lastMove!==false) {
                            //If the subgame the player was sent to is finished, open all boards for playing in.
                            if(game[lastMove[2]][lastMove[3]]!==0&&(i!==lastMove[2]||j!==lastMove[3])&&game[i][j]===0&& resolved === false){
                                td.className += (' valid');
                                img.setAttribute('onclick', 'cellchoice(' + coords + ')');
                            }
                            //If the cell is in the subgame sent to this turn & that game is not finished. 
                            else if (game[lastMove[2]][lastMove[3]]===0 && i === lastMove[2] && j === lastMove[3]&& resolved === false){
                                td.className += (' valid');
                                img.setAttribute('onclick', 'cellchoice(' + coords + ')');
                            }
                        }
                        //Change the styling of the last selected cell.
                        if (i === lastMove[0] && j === lastMove[1] && x === lastMove[2] && y === lastMove[3]){
                            if (board[i][j][x][y] === 1){ // If is was the crosses move last.
                                img.className += (' animated flash');
                            }
                            if (board[i][j][x][y] === 2){// If is was the noughts move last.
                                img.className += (' animated flash');
                            }
                        }
                    }    
                    // If it is the first move.
                    else{
                        if (i === 1 && j === 1){
                            td.className += (' valid');
                            img.setAttribute('onclick', 'cellchoice(' + coords + ')');
                        }
                    }
                    // Set images of cells
                    img.setAttribute('id', coords);
                    switch (board[i][j][x][y]) {
                        case 1:
                            img.setAttribute('src', 'styles/images/Cross.svg');
                            break
                        case 2:
                            img.setAttribute('src', 'styles/images/Nought.svg');
                            break
                        default:
                            break
                    }
                    td.appendChild(img);
                    tr.appendChild(td);
                }
                tbdy.appendChild(tr);
            }
            table.appendChild(tbdy);
            tableWrap.appendChild(table);
            tableWrap.appendChild(tableImg);
            bd.appendChild(tableWrap);
            br.appendChild(bd);
        }
        btbdy.appendChild(br);
   }
   btable.appendChild(btbdy);
   gameDiv.appendChild(btable);
}

//==============================================================================
//                          Game Text Function
//==============================================================================
function gameText() {
    if (checkTie(board)) {
        if (turn === sID) {
            if (checkWin()) {
                $('#gameText').html("<h2>You lost!</h2>");
                resolved=true;
                gameCompleted();		
            }
            else {
                if (gameType === 'private' || gameType === 'public') {
                    if (turnCount % 2 === 0) {
                        $('#gameText').html('<h2><img class="playerSymbol" src="styles/images/Cross.svg"> It is your turn!  <span id="timer"></span> <img class="playerSymbol" src="styles/images/Cross.svg"></h2>');
                    }
                    if (turnCount % 2 === 1) {
                        $('#gameText').html('<h2><img class="playerSymbol" src="styles/images/Nought.svg"> It is your turn!  <span id="timer"></span> <img class="playerSymbol" src="styles/images/Nought.svg"></h2>');
                    }
                }
                else {$('#gameText').html("<h2>Playing against the bot: "+opponentName+"</h2>");}
            }
        }
        else {
            if (checkWin()) {
                $('#gameText').html("<h2>You won!</h2>");
                resolved=true;
                gameCompleted();                
            }
            else {$('#gameText').html("<h2>Waiting for " + opponentName + "'s move <span id='timer'></span></h2>");}
        }
    }
    else {$('#gameText').html("<h2>The game is a tie!</h2>");resolved=true;}
}


function timer(time,update) {
    var start = new Date().getTime();
    interval = setInterval(function() {
        var now = time-(new Date().getTime()-start);
        if( now <= 0) {
            clearInterval(interval);
            if (inGame){
                if (turn === sID) {
                    alert("You took too long to move and forfited!");
                    gameType = "oppenentLeft";
                    quitGame();
                }
                else {
                    alert("Your opponent has left the game!");
                    gameType = "oppenentLeft";
                    quitGame();
                }
            }
        }
        else update(Math.floor(now/1000));
    },100); // the smaller this number, the more accurate the timer will be
}
//==============================================================================
//                        Cell Rendering on Choice 
//==============================================================================
function cellchoice(i,j,x,y) {
    if (board[i][j][x][y] === 0 && checkWin() === false && turn === sID) {
        turn = false;
        rendered = false;
        var coords = [i, j, x, y];
        var img = document.getElementById(coords);
        if (turnCounter() > 0){clearInterval(interval);}
        board[i][j][x][y] = player;
        $.post("GameLogic", {"gameID": gameID,"I":i, "J":j, "X": x, "Y": y}, function() { });
        if (turnCount % 2 === 0) {
            player = 1;
            img.setAttribute('src', 'styles/images/Cross.svg');
        }
        if (turnCount % 2 === 1) {
            player = 2;
            img.setAttribute('src', 'styles/images/Nought.svg');         
        }
    }
}

//==============================================================================
//               Final Board Render When Game Ends
//==============================================================================
function gameCompleted(){
     clearInterval(interval);
     connection.close();
     $('#gameDiv').empty();
     drawTTTTable();
     inGame=false;
     Modal_End();
}