/**
    @Instructions:
    http://nbprithv.github.io/introtour-ui/
**/
var tour_card_width = '400';
var tour_card_height = '20';

function beginTour(){
    //$('.myButton').show();
    toggleButtons('.myButton','open');
    var tour_cards = getBaseTourCards(false);
    if($('#SingleGameBtn').length){
        tour_cards.push({
            'title':'Single Player',
            'content':'Play against the Computer',
            'target':'SingleGameBtn',
            'position':'bottom',
            'width' : tour_card_width,
            'height' : tour_card_height
        });   
    }
    if($('#PublicGameBtn').length){
        tour_cards.push({
            'title':'Public Game',
            'content':'Once a public game has been created, it will be listed for all to view.',
            'target':'PublicGameBtn',
            'position':'bottom',
            'width' : tour_card_width,
            'height' : tour_card_height
        });
    }
    if($('#PrivateGameBtn').length){
        tour_cards.push({
            'title':'Private Game',
            'content':'Private games may only be joined by a Hyperlink, or by those who have the \"GameID\"',
            'target':'PrivateGameBtn',
            'position':'bottom',
            'width' : tour_card_width,
            'height' : tour_card_height
        });
    }
    if($('#replay').length){
        tour_cards.push({
            'title':'Game Replays',
            'content':'Insert the GameID, into the provided input box to replay a game or wait until a game has finished.',
            'target':'replay',
            'position':'bottom',
            'width' : tour_card_width,
            'height' : tour_card_height
        });   
    }
    if($('#BtnNewBot').length){
        tour_cards.push({
            'title':'New Bot',
            'content':'Create a new Bot, and assign a Name.',
            'target':'BtnNewBot',
            'position':'bottom',
            'width' : tour_card_width,
            'height' : tour_card_height
        }); 
    }
    
    if($('#BtnNewBot').length){
        tour_cards.push({
            'title':'Load Bot',
            'content':'Load a previously created bot.',
            'target':'BtnLoadBot',
            'position':'bottom',
            'width' : tour_card_width,
            'height' : tour_card_height
        }); 
    }
    
    if($('#BtnSaveBot').length){
        tour_cards.push({
            'title':'Save Bot',
            'content':'Save a Bot, that you\'ve written.',
            'target':'BtnSaveBot',
            'position':'bottom',
            'width' : tour_card_width,
            'height' : tour_card_height
        }); 
    }
    
    if($('#BtnCompileBot').length){
        tour_cards.push({
            'title':'Compile and Test',
            'content':'Compile the Bot, that you\'ve written.',
            'target':'BtnCompileBot',
            'position':'bottom',
            'width' : tour_card_width,
            'height' : tour_card_height
        }); 
    }
    
    if($('#BtnPlayBot').length){
        tour_cards.push({
            'title':'Play Bot',
            'content':'Play the Bot, that you\'ve written.',
            'target':'BtnPlayBot',
            'position':'bottom',
            'width' : tour_card_width,
            'height' : tour_card_height
        }); 
    }
    
    tour_cards = closeTourCards(tour_cards);
    //init the Help Options
    $().introTour(tour_cards);
    
    //subscribe to events
    document.addEventListener('introTourClosed', function(e){
        if(isLobby && !isBot){
           toggleButtons('.myButton','close');
        }
    });
}
var giveURL, lobbyPath, botPath, baseURL, isLobby, isBot;

$(document).ready(function() {
    //Get a Base path and generate a hyperlink for the current page
    var loc = window.location.pathname;
    var dir = loc.substring(0, loc.lastIndexOf('/'));
    
    //Get the paths
    baseURL = window.location.protocol + '//' + window.location.host + dir + "/";
    giveURL = window.location.protocol + '//' + window.location.host + window.location.pathname;
    lobbyPath = baseURL + $('#lobbyLink').attr('href');
    botPath = baseURL + $('#botLink').attr('href');
    
    //Check if we are on the lobby or bot
    isLobby = lobbyPath == giveURL;
    isBot = botPath == giveURL;
});

function getBaseTourCards(automatic_close){
    if(isLobby){
         toggleButtons('.myButton','open');   
    }    
    var tour_cards = [{
        'title':'Welcome',
        'position':'pagecenter',
        'content':'Welcome to Noughts and Crosses, developed at Massey University'
    }];
    var sessionId = sID = $.cookie('sessionID');
    if (typeof sessionId === 'undefined' || sessionId === null) {
        tour_cards.push({
            'title':'Authentication',
            'content':'Login in with either <a href="#" onclick="FacebookLogin();" class="facebook">Facebook</a> Or <a href="#" onclick="googleLogin();" class="google">Google</a>.',
            'target':'reviewLogin',
            'position':'bottom',
            'width' : tour_card_width,
            'height' : tour_card_height
        });
    }
    if(!isLobby)
    {
        tour_cards.push({
            'title':'Lobbies',
            'content':'After logging in, you can view or create games!',
            'target':'lobbyLink',
            'position':'bottom',
            'width' : tour_card_width,
            'height' : tour_card_height
        });
    }
    if(!isBot) {
        tour_cards.push({
            'title':'Bot Editor',
            'content':'Create a virtual CPU and have other players try to beat it!',
            'target':'botLink',
            'position':'bottom',
            'width' : tour_card_width,
            'height' : tour_card_height
        });
    }
    
    if(typeof automatic_close !== 'undefined' && automatic_close)
    {
        tour_cards = closeTourCards(tour_cards);
        
    }

    return tour_cards;
}

function closeTourCards(tour_cards){
    tour_cards.push(
    {
        'title':'That\'s it! You\'re good to go!',
        'position':'pagecenter'
    });

    return tour_cards;
}