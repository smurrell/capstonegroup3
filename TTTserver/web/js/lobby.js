//===============================================================================
//                              Lobby Functions
//===============================================================================

// Lobby Globals
var inGame, inLobby, sID, gID, connection, gameType, opponentName, modal, interval;
var rendered = false;

//===============================================================================
//                              Connection Functions
//===============================================================================
function createSSEConnection() {
    gameURL = "UpdateBoard/" + gameID;
    connection = new EventSource(gameURL);
    connection.onmessage = function(event) {
        window.onbeforeunload = confirmOnPageExit;
        inGame = true;
        $("#gameListText").hide();
        $('#gameList').hide();
        $('#gameWrap').show();
        data = JSON.parse(event.data);
        newBoard = data.gameArray.valueOf();
        if (data.active.valueOf() === false) {
                connection.close();
                alert("Sorry the other person has left the game");
                inGame = false;
                inLobby = true;
                window.onbeforeunload = null;
                $('#btnCloseGame').hide();
                $('#gameDiv').empty();
                $('#gameWrap').hide();
                $('#gameText').empty();
                lobbyControl();
                pollingTimer();
                clearInterval(interval);
            }
        if ((rendered !== true) || boardsEqual(board, newBoard)) {
            inLobby = false;
            lastMove = data.lastMove.valueOf();
            opponentName = data.opponent.valueOf();
            board = newBoard;
            turn = data.currentTurn.valueOf();
            turnCount = turnCounter();
            $('#gameDiv').empty();
            drawTTTTable();
            gameText();
            rendered = true;
            if (gameType === 'public' || gameType === 'private'){
                if (turnCounter() > 0){clearInterval(interval);}
                timer(60000,function(timeleft) {
                if (inGame){
                    document.getElementById('timer').innerHTML = timeleft+" second(s)";                    
                }
            });
            }

        }
    };
}

function createGame(value) {
    gameType = value;
    toggleButtons('.myButton', 'close');
    sID = $.cookie('sessionID');
    if (typeof sID === 'undefined' || sID === null) {
        modal.open({content: login});
        return false;
    }
    if (inGame) {
        alert("You are currently in a game. Please finish the game to join another :)");
        return false;
    }
    quitGame(); // Delete any games being hosted
    $('#lobbyControl').html('<input type="submit" value="Close Game" id="btnCloseGame" name="btnCloseGame" class="otherButton" onclick ="javascript: quitGame();"/>');
    $.post("Lobby", {"gameType": gameType}, function(result) {
        replay = false;
        rendered = false;
        resolved = false;
        gameID = result;
        gameURL = "UpdateBoard/" + gameID;
        createSSEConnection();
        inGame = true;
        inLobby = true;
        switch (gameType) {
            case 'private':
                $('#gameText').html("<p><h2>Waiting for other player to join your Private Game</h2>");
                $('#gameText').append('Click game link button for the link and send to your friend</p>');
                break;
            case 'public':
                $('#gameText').html("<p><h2>Waiting for other player to join your Public Game</h2>");
                $('#gameText').append('or Click game link button for the link and send to your friend</p>');
                break;
        }
        if (gameType === 'public' || gameType === 'private') {
            var giveURL = window.location.protocol + '//' + window.location.host + window.location.pathname + '?gameID=' + gameID;
            //Add the clipboard button
            var clipboardbutton = $('<input type="submit" value="Game Link" id="GameLinkBtn" name="btnSingle" class="otherButton"  onclick ="javascript: copyToClipboard(\'' + giveURL + '\'); return false;"/>');
            //Append the clipboard button
            $('#gameText').append(clipboardbutton);
        }
    });
}


function joinGame(gID) {
    sID = $.cookie('sessionID');
    if (typeof sID === 'undefined' || sID === null) {
        $(window).load(function(){
                        modal.open({content: login});
                });
        return false;
    }
    if (inGame) {
        alert("You are currently  waiting for someone to joing your's. Please close your prior to joining someone else's");
        return false;
    }
    quitGame();
    $('#lobbyControl').html('<input type="submit" value="Close Game" id="btnCloseGame" name="btnCloseGame" class="otherButton" onclick ="javascript: quitGame();"/>');
    gameID = gID;
    rendered = false;
    resolved = false;
    gameType = "public";
    $.post("Lobby", {"sID": sID, "gameID": gameID}).always(function(data) {
        inGame = true;
        replay = false;
        gameURL = "UpdateBoard/" + gameID;
        createSSEConnection();
    });
}

// Join private game by URL
$(document).ready(function() {
    var urlQuery = getUrlVars();
    //if we have the GameID! in the GET protocol.
    if (urlQuery.length > 0 && typeof urlQuery['gameID'] !== 'undefined' && urlQuery['gameID'].length > 0) {
        joinGame(urlQuery['gameID']);
    }
    else if (urlQuery.length > 0 && typeof urlQuery['botID'] !== 'undefined' && urlQuery['botID'].length > 0) {
        createGame(urlQuery['botID']);
    }
    var loc = window.location.pathname;
    var dir = loc.substring(0, loc.lastIndexOf('/'));
    //Get the path to ZeroClipBoard
});

function quitGame() {
    if (inGame &&(gameType === 'private' || gameType === 'public')) {
        if (confirm("Abandoning games ruins the experience for other players, please reconsider")) {
            clearInterval(interval);
            $.post("Lobby", {"quitID": gameID});
            clearGameStuff();
            
        }
        else {return false;}
    }
    //@TODO Duplicated code.
    else {clearGameStuff();}
    lobbyControl();
}

function clearGameStuff(){
    window.onbeforeunload = null;
    game = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
    replay = false;
    $('#btnCloseGame').hide();
    $('#gameDiv').empty();
    $('#gameWrap').hide();
    $('#gameText').empty();
    pollingTimer();
    if (inGame){
        connection.close();
    }
    inGame = false;

    
}


var confirmOnPageExit = function(e) {
    e = e || window.event;
    var message = "Please use Close Game";
    // For IE6-8 and Firefox prior to version 4
    if (e) {
        e.returnValue = message;
    }
    // For Chrome, Safari, IE8+ and Opera 12+
    return message;
};

function boardsEqual(board1, board2) {
    if (board2 === undefined){
        return false;
    }
    if(board1.length !== board2.length)
        return false;
    for(var i = board1.length; i--;) {
        for(var j = board1.length; j--;) {
            for(var x = board1.length; x--;) {
                for(var y = board1.length; y--;) {
                    if(board1[i][j][x][y] !== board2[i][j][x][y]){
                        return true;
                    }
                }      
            }
        }
    }
    return false;
}

function pollingTimer() {
    inLobby = true;
    refreshList();
    var refreshTiming = setInterval(function() {
        if (document.hasFocus()) {
            refreshList();
        }
        if (inGame && !inLobby) {
            clearInterval(refreshTiming);
        }
    }, 1000);
}

function refreshList() {
    // getopendiv
    // load div from list of div
    $.get("Lobby", {}, function(data) {
        if (!inLobby) {
            $('#gameList').hide();
            return;
        }
        // set as content in div with id conversations
        var div = document.getElementById('gameList');
        div.innerHTML = "";
        //Check that the data exists and there are games to list.
        if (data !== null && data.game.length > 0) {
            var gamelist = data;
            gamelist = gamelist.game;
            for (i = 0; i < gamelist.length - 1; i++) {
                var name = gamelist[i].name;
                var gameID = gamelist[i].gameID;
                var box = document.createElement('div');
                box.setAttribute('class', 'gameRow');
                box.setAttribute('onclick', "joinGame(\'" + gameID + "\')");
                var text = document.createElement('text');
                text.innerHTML = "Play against " + name;
                box.appendChild(text);
                div.appendChild(box);
            }
            //if there are no games, note that its 1 because of the proto thing thats in the default list.
            if (gamelist.length === 1) {
                $("#gameList").hide();
                $("#gameListText").show();
                $("#gameListText").html("<h3>No public games are currently open. Click create game to make one</h3>");
            }
            if (gamelist.length !== 1) {
                $("#gameList").show();
                $("#gameListText").show();
                $("#gameListText").html("<h2>Open Games</h2>");
            }

        }
    });
}

function copyToClipboard(text) {
    window.prompt("Send this link to your friend to allow them to join \n\ Copy to clipboard: Ctrl+C, Enter", text);
}

//////////////////////
/*Replay Functions*/
//////////////////////

function replayGame() {
    clearInterval(interval);
    if (inGame) {
        alert("You are currently in a game, or are waiting for someone to joing your's. Please wait :)");
        return false;
    }
    close_modal();
    resolved=false;
    gameType='replay';
    replay = true;
    inGame = true;
    $("#gameList").hide();
    $.get("Lobby", {"gameID": gameID}, function(data) {
        var gameMoveArray = [];
        for (var n = 0; n < data.length; n++) {
            var i = data[n].i;
            var j = data[n].j;
            var x = data[n].x;
            var y = data[n].y;
            var playerName = data[n].nickName;
            gameMoveArray.push([i, j, x, y, playerName]);
        }
        board = [[[[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]]], [[[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]]], [[[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]]]];
        game = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
        $('#gameDiv').empty();
        $('#gameText').empty();
        for (var m = 0; m < gameMoveArray.length; m++) {
            replayAnimation(m, gameMoveArray);
        }
        setTimeout(function() {gameType = false;replay = false;}, 1000 * gameMoveArray.length);
    });
}
;

function replayAnimation(i, gameMoveArray) {
    setTimeout(function() {
        if (replay){
            player = turnCounter() % 2 + 1;
            coords = gameMoveArray[i];
            playerName = coords[4];
            lastMove = coords;
            board[coords[0]][coords[1]][coords[2]][coords[3]] = player;
            $('#gameDiv').empty();
            drawTTTTable();
            $('#gameText').html("<h2>Turn: " + turnCounter() + ". Player " + playerName + " just moved</h2>");
            //Update board text based on outcome.
            if (checkWin()) {
                $('#gameText').html("<h2>Player " + playerName + " won the game!</h2>");
                inGame = false;
                Modal_End();
            }
            else if (checkWin() !== true && turnCounter() === gameMoveArray.length) {
                $('#gameText').html("<h2>The game was a draw!</h2>");
            }
        }
    }, 1000 * (i + 1));
}



// Read a page's GET URL variables and return them as an associative array.
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function toggleButtons(btnName, state) {
    switch (state) {
        case 'open':
            $(btnName).show();
            if (btnName === '.myButton') {
                $(".dnthide").attr("value", "Hide Menu");
            }
            break;
        case 'close':
            $(btnName).hide();
            if (btnName === '.myButton') {
                $(".dnthide").attr("value", "Create Game");
            }
            break;
        case undefined:
            
            $(btnName).toggle();
            if (btnName === '.myButton') {
                if ($(".dnthide").attr("value") === "Create Game") {
                    $(".dnthide").attr("value", "Hide Menu");
                }
                else
                {
                    $(".dnthide").attr("value", "Create Game");
                }
            }
            break;
    }   
}

function lobbyControl() {
    $('#lobbyControl').html(
            "<input type=\"submit\" value=\"Create Game\" name=\"btnToggle\" class=\"dnthide\" onclick =\"javascript: toggleButtons('.myButton'); return false;\"\>" +
            "<input type=\"submit\" value=\"Singleplayer Game\" id=\"SingleGameBtn\" name=\"btnSingle\" class=\"myButton\" style=\"display: none;\" onclick =\"javascript: createGame('1'); toggleButtons('.myButton', 'close'); return false;\"\>" +
            "<input type=\"submit\" value=\"Public Multiplayer Game\" id=\"PublicGameBtn\" name=\"btnPublic\" class=\"myButton\" style=\"display: none;\" onclick =\"javascript: createGame('public'); toggleButtons('.myButton', 'close'); return false;\"\>" +
            "<input type=\"submit\" value=\"Private Multiplayer Game\" id=\"PrivateGameBtn\" name=\"btnPrivate\" class=\"myButton\" style=\"display: none;\" onclick =\"javascript: createGame('private'); toggleButtons('.myButton', 'close'); return false;\" \>"
            );
}