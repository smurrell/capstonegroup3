var editor, botName;
var complied = false;

function buildEditor() {
    editor = ace.edit("editor");
    editor.setTheme("ace/theme/ambiance");
    editor.getSession().setMode("ace/mode/java");
}

function newBot() {
    localStorage.setItem('botName', botName);
    $.post("BotServlet", {"method": "new", "botName": botName}, function(data) {
        console.log(data.valid);
        if (data.valid.valueOf()) {
            loadBot();
        }
    });

}
function vaildateBotName(tempBotName) {
    document.getElementById("btnSubmitNewBot").disabled = false;
    $.post("BotServlet", {"method": "validateName", "botName": tempBotName}, function(data) {
        if (data.valid.valueOf()) {
            $('#newbotimg').html('<img id="newBotImg" src="styles/images/tick.jpg" />');
            $('#validationText').html('Name available');
            document.getElementById("btnSubmitNewBot").disabled = false;
            botName = tempBotName;
        }
        else {
            $('#newbotimg').html('<img id="newBotImg" src="styles/images/cross.jpg" />');
            $('#validationText').html('Name unavailable');
            document.getElementById("btnSubmitNewBot").disabled = true;
        }


    });
}
function loadBot(url) {
    $('#IDEConsole').html('>>>');
    close_modal();
    console.log(url);
    if (typeof url !== 'undefined' && url !== null) {
        botName = url.split('/')[1];
    }
    else {
        var url = botName;
        //player ID bit
    }
    localStorage.setItem('botName', botName);
    $.get("BotServlet", {"method": "load", "path": url}, function(data) {
        localStorage.setItem('BotCode', data);
        localLoad();
    });
}

function getBots() {
    sID = $.cookie('sessionID');
    if (typeof sID === 'undefined' || sID === null) {
        //call modal to prompt user to login.
        modal.open({content: login});
        return false;
    }

    $.get("BotServlet", {"method": "list"})
            .done(function(data) {
                if (data !== null && data.bot.length > 0) {
                    var botlist = document.createElement('div');
                    botlist.setAttribute("id", "botList");
                    var botList = data.bot;
                    for (i = 0; i < botList.length - 1; i++) {
                        var name = botList[i].botName;
                        var url = botList[i].url;
                        var box = document.createElement('div');
                        box.setAttribute('class', 'gameRow');
                        box.setAttribute('onclick', "loadBot('" + url + "')");
                        var text = document.createElement('text');
                        text.innerHTML = "Load This Bot: " + name;
                        box.appendChild(text);
                        botlist.appendChild(box);
                    }
                    $('#loadBot').innerHTML = "";
                    $('#loadBot').append(botlist);
                    //if there are no games, note that its 1 because of the proto thing thats in the default list.
                }
                modal.open({content: document.getElementById('botList')});

            })
}

function saveBot() {
    sID = $.cookie('sessionID');
    if (typeof sID === 'undefined' || sID === null) {
        //call modal to prompt user to login.
        modal.open({content: login});
        return false;
    }
    localStorage.setItem('BotCode', getBotCode());
    localLoad();
    $.post("BotServlet", {"method": "save", "filename": botName.toString(), "code": getBotCode()}, function(data) {
    });
}

function compileBot() {
    sID = $.cookie('sessionID');
    if (typeof sID === 'undefined' || sID === null) {
        //call modal to prompt user to login.
        modal.open({content: login});
        return false;
    }
    localStorage.setItem('BotCode', getBotCode());
    localLoad();
    $('#IDEConsole').html('>>>');
    $.post("BotServlet", {"method": "compile", "filename": botName.toString(), "code": getBotCode()}, function(data) {
        if (data.compiled.valueOf()) {
            if (data.valid.valueOf()) {
                $('#IDEConsole').html('>>><br/><span id="consolseText">Successfully Complied</span>');
                complied = true;
            }
            else {
                $('#IDEConsole').html('>>><br/><span id="consolseText">Successfully Complied, but REJECTED due to invalid API/library function usage</span>');
                complied = false;
            }

        }
        else {
            var errorMessage = data.errorMessage.valueOf();
            $('#IDEConsole').html('>>><br/><span id="consolseText">line: ' + errorMessage.lineNumber + '</span><span id="errorMessage"> ' + errorMessage.message + '</span>');
        }
    });
}

function testBot() {
    sID = $.cookie('sessionID');
    if (typeof sID === 'undefined' || sID === null) {
        //call modal to prompt user to login.
        modal.open({content: login});
        return false;
    }
    localStorage.setItem('BotCode', getBotCode());
    localLoad();

    
    if (complied) {
        var botURL = window.location.protocol + '//' + window.location.host + (location.pathname.split('/').slice(0, -1).join('/')) + '/lobby.html?botID=' + botName;
        window.location.replace(botURL);
    }
    else{
        $('#IDEConsole').html('>>><br/><span id="errorMessage">Complie bot first</span>');
    }
}

function getBotCode() {
    return editor.getSession().getValue().toString();
}

function localLoad() {
    sID = $.cookie('sessionID');
    if (typeof sID === 'undefined' || sID === null) {
        editor.getSession().setValue('');
        botName = '';
    }
    else {
        editor.getSession().setValue(localStorage.getItem('BotCode'));
        botName = localStorage.getItem('botName');
    }

}


function saveTimer() {
    localStorage.setItem('BotCode', getBotCode());
    console.log('save');
    setInterval(function() {
        if (document.hasFocus()) {
            console.log('autosave');
            localStorage.setItem('BotCode', getBotCode());
        }

    }, 5000);
}