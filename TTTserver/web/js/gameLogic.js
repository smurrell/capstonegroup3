var board = [[[[0,0,0],[0,0,0],[0,0,0]],[[0,0,0],[0,0,0],[0,0,0]],[[0,0,0],[0,0,0],[0,0,0]]],[[[0,0,0],[0,0,0],[0,0,0]],[[0,0,0],[0,0,0],[0,0,0]],[[0,0,0],[0,0,0],[0,0,0]]],[[[0,0,0],[0,0,0],[0,0,0]],[[0,0,0],[0,0,0],[0,0,0]],[[0,0,0],[0,0,0],[0,0,0]]]];
var game = [[0,0,0],[0,0,0],[0,0,0]];
var replay = false;
var resolved = false;
var timeLimit = 60;
var turnCount, turn, lastMove, player, gameID;


//==============================================================================
//                         Table Win Cases
//==============================================================================
//Check each individual table
function checkTableWin(i,j) {
    // horizontal win
    for (var x = 0; x < 3; x++) {
        if (board[i][j][x][0] === board[i][j][x][1] && board[i][j][x][1] === board[i][j][x][2] && board[i][j][x][0] !== 0) {
            game[i][j]=board[i][j][x][0];
            return true;
        }
    }
    //vertical win
    for (var y = 0; y < 3; y++) {
        if (board[i][j][0][y] === board[i][j][1][y] && board[i][j][1][y] === board[i][j][2][y] && board[i][j][0][y] !== 0) {
            game[i][j]=board[i][j][0][y];
            return true;
        }
    }
    //diagonal win
    if (board[i][j][0][0] === board[i][j][1][1] && board[i][j][1][1] === board[i][j][2][2] && board[i][j][0][0] !== 0) {
        game[i][j]=board[i][j][0][0];
        return true;
    }
    if (board[i][j][0][2] === board[i][j][1][1] && board[i][j][1][1] === board[i][j][2][0] && board[i][j][0][2] !== 0) {
        game[i][j]=board[i][j][0][2];
        return true;
    }
    return false;
}


//==============================================================================
//                     Board Win Cases
//==============================================================================

function checkWin(){
    for(i=0; i<3; i++){
            for(j=0;j<3; j++){
                    if (checkTie(board[i][j]) === false){
                        game[i][j] = 3;
                    }
                    checkTableWin(i,j);
                }
            }
    //Check for Horizontal wins in each row
    for(var x=0; x<3; x++){
            //If they are not all 0s
            if((game[x][0]!==0&&game[x][1]!==0&&game[x][2]!==0)&&((game[x][0]!==3||game[x][1]!==3)||game[x][2]!==3)){
                    // First = Second || Tie. Second = Third || Tie. Third = First ||Tie
                    if((((game[x][0]===game[x][1])||(game[x][0]===game[x][2]))||(game[x][0]===3))&&(((game[x][1]===game[x][2])||(game[x][1]===game[x][0]))||(game[x][1]===3)) &&(((game[x][2]===game[x][0])||(game[x][2]===game[x][1]))||(game[x][2]===3))){
                            return true;
                    }
            }
    }
    //Check for Vertical wins in each column
    for(var y=0; y<3; y++)
    {       
            //If any are not ties, and if they are not all 0s
            if((game[0][y]!==0&&game[1][y]!==0&&game[2][y]!==0)&&((game[0][y]!==3||game[1][y]!==3)||game[2][y]!==3)){
                // First = Second || Tie. Second = Third || Tie. Third = First ||Tie
                if((((game[0][y]===game[1][y])||(game[0][y]===game[2][y]))||(game[0][y]===3))&&(((game[1][y]===game[2][y])||(game[1][y]===game[0][y]))||(game[1][y]===3)) &&(((game[2][y]===game[0][y])||(game[2][y]===game[1][y]))||(game[2][y]===3))){
                        return true;
                    }
            }
    }

    //Check the 2 Diagonal win cases
    //If any of the cells are not all ties and all of the cells are not 0.
    if ((game[0][0]!==0&&game[1][1]!==0&&game[2][2]!==0)&&((game[0][0]!==3||game[1][1]!==3)||game[2][2]!==3)){
            //First = Second || Tie . Second = Third ||Tie. Third = First ||Tie 
            if((((game[0][0]===game[1][1])||(game[0][0]===game[2][2]))||(game[0][0]===3))&&(((game[1][1]===game[2][2])||(game[1][1]===game[0][0]))||(game[1][1]===3)) &&(((game[2][2]===game[0][0])||(game[2][2]===game[1][1]))||(game[2][2]===3))){
                    //If any are not ties, and if they are not all 0s.
                    return true;
            }
    }
    if ((game[0][2]!==0&&game[1][1]!==0&&game[2][0]!==0)&&((game[0][2]!==3||game[1][1]!==3)||game[2][0]!==3)){
            //First = Second || Tie . Second = Third ||Tie. Third = First ||Tie 
            if((((game[0][2]===game[1][1])||(game[0][2]===game[2][0]))||(game[0][2]===3))&&(((game[1][1]===game[2][0])||(game[1][1]===game[0][2]))||(game[1][1]===3)) &&(((game[2][0]===game[0][2])||(game[2][0]===game[1][1]))||(game[2][0]===3))){
                    //If any are not ties, and if they are not all 0s.
                    return true;
            }
    }
    return false;
}

//==============================================================================
//                     Table Tie Check
//==============================================================================
function checkTableNotTie(i,j) {
    //pass it a board to look at.
    found = [];
    //Make a string version of the board.
    var stringb = board.toString();
    //Trim the string of commas, and parenthasies. resulting string is all #'s
    stringb=stringb.replace(/,/g,"");
    stringb=stringb.substr(((i*9)+(j*27)),9);
    found.push((stringb.indexOf(0) !== -1));
    //If the game is a tie:
    if(found.indexOf(true) === -1) {
            game[i][j]=3;
            return true;
        }
    return false;
}

//==============================================================================
//                     Board Tie Check
//==============================================================================
function checkTie(theBoard) {
    found = [];
    var stringg = theBoard.toString();
    found.push((stringg.indexOf(0) !== -1));
    return (found.indexOf(true) !== -1);
}

//==============================================================================
//                     Turn Counter
//==============================================================================
function turnCounter() {
    var counter = 0;
    for (var i = 0; i < board.length; i++) {
        for (var j = 0; j < board.length; j++) {
            for (var x = 0; x < board.length; x++) {
                for (var y = 0; y < board.length; y++) {
                    if (board[i][j][x][y] !== 0) {
                        counter = counter + 1;
                    }
                }
            }
        }
    }
    turnCount = counter;
    return counter;
}