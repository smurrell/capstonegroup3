<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->

<head>
    <title>Home</title>

    <link rel="stylesheet" href="styles/style.css">
    <link rel="shortcut icon" href="styles/images/favicon.ico"/>
    <!-- <script type='text/javascript' src='js/jquery.min.js'></script> -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type='text/javascript' src='js/login/sonic.js'></script>
    <script type='text/javascript' src='js/login/sonic.loaders.js'></script>
    <script type='text/javascript' src='js/login/hmac-sha256.js'></script>
    <script type='text/javascript' src='js/login/login.js'></script>
    <script type='text/javascript' src='js/login/login.google.js'></script>
    <script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-Frame-Options" content="deny">
    <script type="text/javascript">
        $(function() {
        $('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });		
        });
    </script>
</head>
<body>
    <img class="logo" src='styles/images/logo.png' />

    <h2 class="menu"><a href="index.jsp">  Home  </a> - <a rel="leanModal" href="#Login" id="reviewLogin" >  Login  </a> - <a href="lobby.html">  Lobby  </a> - <a href="ide.html"> Bot Editor </a></h2>
    <div id="primary_wrapper">
        <h1>Home</h1>
        <div class="left space"></div>
        <div class="right">
            <h3>Welcome!</h3>
            <p>This site has been created by a group at Massey University for the Capstone course. You can navigate and login above.</p>
            <p>The current game follows a nested design with specialised rules.</p>
            <hr>
            <h3>The Rules:</h3>
            <p>Note: These are yet to be implemented and are here for a reference.</p>
            <ol>
            <li>Player 1 must start in the middle board. (This helps to prevent start bias)</li>
            <li>Whichever square in the sub-board player 1 chooses, is the sub-board the other player must play in.</li>
            <li>First to three subgames in a row/column/diagonal wins</li> 
            
        </div>
        <!--Hidden login div for overlay-->
        <div id="Login"  href="Login">
            <h3>Login</h3>
            <p>Welcome! To get playing please login using either link below.</p>
            <div id="fb-root"></div>
            <script type='text/javascript' src='js/login/login.facebook.js'></script>
            <form id="LoginForm" action="authapi" method="post" onsubmit="return false" >
                <ul><li class="hidden alert" id="formAlert"></li></ul>
                <div id="LoginFormOptions" >
                    <div id="socialMediaWrapper">
                        <div class="socialMediaOauth">
                            <a href="#" class="fb" id="sm_fb" title="Facebook Login" onclick="FacebookLogin();" ></a>
                            <a href="#" class="yt" id="sm_yt" title="Google Login" onclick="googleLogin()"></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>